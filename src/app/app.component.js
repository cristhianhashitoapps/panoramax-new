var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from 'ng2-ui-auth';
import { Events } from 'ionic-angular';
import { UserFactory } from '../providers/UserFactory';
import * as rootScope from './rootScope';
import { HomePage } from '../pages/home/home';
import { PerfilPage } from '../pages/perfil/perfil';
var $rootScope = rootScope.default;
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, events, userFactory) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.events = events;
        this.userFactory = userFactory;
        this.rootPage = HomePage;
        this.initializeApp();
        this.user = { Proveedor: false };
        this.events.subscribe('user:logued', function (user) {
            _this.user = $rootScope.user;
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Inicio', component: HomePage },
            { title: 'Perfil', component: PerfilPage }
        ];
        console.log(auth.isAuthenticated());
        //let env = this;
        if (auth.isAuthenticated()) {
            this.user = $rootScope.user = auth.getPayload().sub;
            userFactory
                .CargarUsuario($rootScope.user.Id)
                .subscribe(function (result) {
                var user = result;
                user.Completo =
                    !!user.Identificacion &&
                        !!user.Telefono &&
                        !!user.TelefonoWhatsApp &&
                        !!user.Direccion;
                user.Telefono = user.Telefono * 1;
                user.Identificacion = user.Identificacion * 1;
                user.TelefonoWhatsApp = user.TelefonoWhatsApp * 1;
                _this.user = $rootScope.user = user;
                console.log('$rootScope: ', $rootScope);
            }, function (error) {
                console.log(error);
            });
        }
        this.rootPage = !auth.isAuthenticated() ? 'SignUp' : 'Home';
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.CerrarSesion = function () {
        console.log("cerro sesion");
        this.auth.logout();
        this.nav.setRoot('SignUp');
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform,
            StatusBar,
            SplashScreen,
            AuthService,
            Events,
            UserFactory])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map