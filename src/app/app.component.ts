import { Component, ViewChild } from '@angular/core';
import { Nav, Platform }        from 'ionic-angular';
import { StatusBar }            from '@ionic-native/status-bar';
import { SplashScreen }         from '@ionic-native/splash-screen';
import { AuthService }          from 'ng2-ui-auth';
import { Events }               from 'ionic-angular';
import { UserFactory}           from '../providers/UserFactory';
import * as rootScope           from './rootScope';

import { HomePage }             from '../pages/home/home';
import { ListPage }             from '../pages/list/list';
import { PerfilPage }           from '../pages/perfil/perfil';
import { SignUp }               from '../pages/signup/signup';
import { ValidatorPage }        from '../pages/validator/validator';
import { PedidosPage }          from '../pages/pedidos/pedidos';
import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';


const $rootScope = rootScope.default;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  user: any;

  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth: AuthService,
    public events: Events,
    public userFactory: UserFactory,
    private fcm: FCM,
    private localNotifications: LocalNotifications) {

    this.initializeApp();

    this.user = { Proveedor: false };

    this.events.subscribe('user:logued', (user) => {
      this.user = $rootScope.user;
    });

    // used for an example of ngFor and navigation
    this.pages = [
    { title: 'Inicio', component: HomePage },
    { title: 'Perfil', component: PerfilPage }
    ];


    console.log(auth.isAuthenticated());
    //let env = this;

    if (auth.isAuthenticated()) {
      this.user = $rootScope.user = auth.getPayload().sub;

      userFactory
      .CargarUsuario($rootScope.user.Id)
      .subscribe(result => {
        let user: any = result;
        user.Completo =
        !!user.Identificacion &&
        !!user.Telefono &&
        !!user.TelefonoWhatsApp &&
        !!user.Direccion;

        user.Telefono         = user.Telefono * 1;
        user.Identificacion   = user.Identificacion * 1;
        user.TelefonoWhatsApp = user.TelefonoWhatsApp * 1;

        this.user = $rootScope.user = user;

        console.log('$rootScope: ', $rootScope);
      },
      error => {
        console.log(error);
      });
    }
    this.rootPage = !auth.isAuthenticated() ? 'SignUp' : 'HomePage';
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      //notificacion
      this.fcm.onNotification().subscribe(data=>{
        if(data.wasTapped){

          // Schedule a single notification
          this.localNotifications.schedule({
          id: 1,
          text: data.mensaje,
          });

        } else {
          // Schedule a single notification
          this.localNotifications.schedule({
          id: 1,
          text: data.mensaje,
        });

        };
      })
      //notificacion

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  CerrarSesion(){
    console.log("cerro sesion");
    this.auth.logout();
    this.nav.setRoot('SignUp');
  }
}
