var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Ng2UiAuthModule } from 'ng2-ui-auth';
import { CustomConfig } from 'ng2-ui-auth';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { Toast } from '@ionic-native/toast';
import { NativeStorage } from '@ionic-native/native-storage';
import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';
import { OrtodonciaCirugiaPage } from '../pages/ortodoncia-cirugia/ortodoncia-cirugia';
import { CategoriesPage } from '../pages/categories/categories';
import { RadiologiaTomografiaPage } from '../pages/radiologia-tomografia/radiologia-tomografia';
import { TomografiaFotografiaPage } from '../pages/tomografia-fotografia/tomografia-fotografia';
import { PacientePageModule } from '../pages/paciente/paciente.module';
import { EnvioPageModule } from '../pages/envio/envio.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { HomePageModule } from '../pages/home/home.module';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Config } from '../providers/config';
import { UserFactory } from '../providers/UserFactory';
import { SignUpFactory } from '../providers/SignUpFactory';
import { CategoriaFactory } from '../providers/CategoriaFactory';
import { TipoEntregaResultadosFactory } from '../providers/TipoEntregaResultadosFactory';
import { BannerFactory } from '../providers/BannerFactory';
import { AutoCompleteModule } from 'ionic2-auto-complete';
var MyAuthConfig = /** @class */ (function (_super) {
    __extends(MyAuthConfig, _super);
    function MyAuthConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.loginUrl = [Config.ApiUrl, 'Doctor/login'].join('/');
        _this.signupUrl = [Config.ApiUrl, 'Doctor/signup'].join('/');
        _this.tokenName = 'Token';
        _this.tokenPrefix = 'panoramax';
        _this.defaultHeaders = { 'Content-Type': 'application/json' };
        return _this;
    }
    return MyAuthConfig;
}(CustomConfig));
export { MyAuthConfig };
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                ListPage,
                OrtodonciaCirugiaPage,
                CategoriesPage,
                RadiologiaTomografiaPage,
                TomografiaFotografiaPage
            ],
            imports: [
                BrowserModule,
                HomePageModule,
                PacientePageModule,
                EnvioPageModule,
                PerfilPageModule,
                AutoCompleteModule,
                IonicModule.forRoot(MyApp),
                Ng2UiAuthModule.forRoot(MyAuthConfig)
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                ListPage,
                OrtodonciaCirugiaPage,
                CategoriesPage,
                RadiologiaTomografiaPage,
                TomografiaFotografiaPage,
            ],
            providers: [
                StatusBar,
                SplashScreen,
                Transfer,
                TransferObject,
                File,
                NativeStorage,
                Toast,
                UserFactory,
                SignUpFactory,
                BannerFactory,
                CategoriaFactory,
                TipoEntregaResultadosFactory
            ],
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map