import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

import { Ng2UiAuthModule } from 'ng2-ui-auth';
import { CustomConfig } from 'ng2-ui-auth';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from'@ionic-native/file';
import { Toast } from '@ionic-native/toast';

import { NativeStorage } from '@ionic-native/native-storage';

import { MyApp } from './app.component';
import { ListPage } from '../pages/list/list';
import { OrtodonciaCirugiaPageModule } from '../pages/ortodoncia-cirugia/ortodoncia-cirugia.module';
import { CategoriesPageModule } from '../pages/categories/categories.module';
import { RadiologiaTomografiaPageModule } from '../pages/radiologia-tomografia/radiologia-tomografia.module';
import { TomografiaFotografiaPageModule } from '../pages/tomografia-fotografia/tomografia-fotografia.module';
import { PacientePageModule } from '../pages/paciente/paciente.module';
import { EnvioPageModule } from '../pages/envio/envio.module';
import { PerfilPageModule } from '../pages/perfil/perfil.module';
import { HomePageModule } from '../pages/home/home.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Config } from '../providers/config';
import { UserFactory} from '../providers/UserFactory';
import { SignUpFactory } from '../providers/SignUpFactory';
import { CategoriaFactory } from '../providers/CategoriaFactory';
import { TipoEntregaResultadosFactory } from '../providers/TipoEntregaResultadosFactory';

import { BannerFactory } from '../providers/BannerFactory';
import { AutoCompleteModule } from 'ionic2-auto-complete';

import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';



export class MyAuthConfig extends CustomConfig {
  loginUrl       = [Config.ApiUrl, 'Doctor/login'].join('/');
  signupUrl      = [Config.ApiUrl, 'Doctor/signup'].join('/');
  tokenName      = 'Token';
  tokenPrefix    = 'panoramax';

  defaultHeaders = {'Content-Type': 'application/json'};
}



@NgModule({
  declarations: [
    MyApp,
    ListPage
  ],
  imports: [
    BrowserModule,
    HomePageModule,
    OrtodonciaCirugiaPageModule,
    RadiologiaTomografiaPageModule,
    TomografiaFotografiaPageModule,
    PacientePageModule,
    EnvioPageModule,
    PerfilPageModule,
    AutoCompleteModule,
    CategoriesPageModule,
    IonicModule.forRoot(MyApp),
    Ng2UiAuthModule.forRoot(MyAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage,
  ],  
  providers: [
    StatusBar,
    SplashScreen,
    Transfer,
    TransferObject,
    File,
    NativeStorage,
    Toast,
    UserFactory,
    SignUpFactory,
    BannerFactory,
    CategoriaFactory,
    TipoEntregaResultadosFactory,
    FCM,
    LocalNotifications
  ],
})
export class AppModule {}
