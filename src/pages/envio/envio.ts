import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Loading, LoadingController } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import { CategoriaFactory } from '../../providers/CategoriaFactory';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TipoEntregaResultadosFactory} from '../../providers/TipoEntregaResultadosFactory';
import { HomePage } from '../home/home';

import * as rootScope from '../../app/rootScope';

const $rootScope = rootScope.default;

/**
 * Generated class for the EnvioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'page-envio',
   templateUrl: 'envio.html',
 })
 export class EnvioPage {
   private tiposEntrega;
   private observaciones;
   public usr;
   public direccionPedido;
   public direccionesId;
   public direcciones;
   public profileData;
   public paciente;
   public nav:any;

   constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public tipoEntregaResultadosFactory: TipoEntregaResultadosFactory,
     public signUpFactory: SignUpFactory,
     public categoriaFactory: CategoriaFactory,
     private app: App,
     public loadingCtrl: LoadingController,
     public toastCtrl: ToastController) {

     this.usr = $rootScope.user;
     this.paciente = this.navCtrl.parent.pacienteId;
     this.profileData  = $rootScope.user;
     this.direccionesId = null;
     this.bucarTiposEntrega();
     this.cargarDirecciones();
     this.observaciones = "";
     this.direccionPedido  = null;
     this.direcciones = [];

     console.log("Paciente: ",this.paciente);
     console.log("Doctor: ",this.usr);
   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad EnvioPage');
   }

   bucarTiposEntrega(){

     this.tipoEntregaResultadosFactory
     .GetAllTipoEntregaResultados()
     .subscribe((result: any) => {
       this.tiposEntrega = result.rows;
       console.log("Resultado ", this.tiposEntrega)
     })
     err => {
       console.log("Error - GetAllTipoEntregaResultados: ", err);
       this.mensajeEnToast('Error: ' + err);
     }
   }

   CheckAddress(direccionPedido) {
     console.log("Entro aquiiiiiiiiiiiiiiii");
     this.direccionPedido = direccionPedido;
     console.log("direccionesssssssssss",this.direccionPedido);

     if (this.direccionPedido === 'nva') {
       var direccion = {};
       this.navCtrl.push('DireccionNuevaPage', {
         IdDirecciones: this.direccionesId

       });
       console.log("Dir Nueva", this.direccionesId);
     };
   };

   cargarDirecciones(){
     this.signUpFactory
     .LoadAddress(this.profileData.Id)
     .subscribe((result: any) => {

       this.direcciones = result;
       console.log("------direcciones-----");
       console.log(this.direcciones);
       console.log("------direcciones-----");
     })
     err => console.log("error", err);
   }

   EnviarPedidoPanoramax(){

     var metodoEnvioSeleccionado = false;

     this.tiposEntrega.filter((item) => {
       if (item.selected == true) {
         metodoEnvioSeleccionado = true;
       }
       return null;
     });

     if(metodoEnvioSeleccionado){
       console.log("Metodo envio", metodoEnvioSeleccionado);



         var data = this.navCtrl.parent.pedido;
         console.log(this.direcciones);

         var EmpresaNombre = "";
         if(this.direccionPedido!=undefined){
            for(var dp =0;dp<this.direcciones.length;dp++){
              if(this.direcciones[dp].Id==this.direccionPedido){
                EmpresaNombre = [this.direcciones[dp].tipoDireccion,this.direcciones[dp].Num1,this.direcciones[dp].Num2,this.direcciones[dp].Num3].join("-");
                break;
              }
            }
         }

         var pedido = {
           "DoctorNombre": this.usr.nombres+" "+this.usr.apellidos,
           "DoctorId" : this.usr.Id,
           "DoctorTelefono": this.usr.telefono,
           "DoctorCorreo" : this.usr.correo,
           "PacienteId": this.paciente.Id,
           "PacienteNombre": this.paciente.nombres+" "+ this.paciente.apellidos,
           "PacienteEdad": this.paciente.edad,
           "PacienteTelefono": this.paciente.telefono,
           "PacienteEmail": this.paciente.email,
           "PacienteDocumento": this.paciente.cedula,
           "PacienteFechaNacimiento": this.paciente.fechaNacimiento,
           "observaciones": this.observaciones,
           "TipoServicioId": this.navCtrl.parent.categoriaId,
           "TipoEstadoId": 1,
           "TipoServicioNombre": this.navCtrl.parent.categoriaNombre,
           "EmpresaId": this.direccionPedido,
           "EmpresaNombre": EmpresaNombre
         }

         let tiposEntrega = [];

          this.tiposEntrega.filter((item) => {
               if (item.selected == true) {
                 tiposEntrega.push({
                   Id: item.Id,
                   Nombre: item.nombre
                 })
               }
               return null;
             });
          console.log("tipos de entrega", tiposEntrega);
           var eviarOrdenDireccion = false;
           tiposEntrega.forEach(item => {
              console.log("Entro Aqui",item.Id );
            if(item.Id == 4){
              console.log("Entro Aqui",item.Id );
               eviarOrdenDireccion = true;
              if(this.direccionPedido == null){
                eviarOrdenDireccion = false;
               console.log("Entro Aqui");
              }else{
                eviarOrdenDireccion = true;

              }
           }else{
             eviarOrdenDireccion = true;
           }
       });
            console.log("estadoooo.....", eviarOrdenDireccion);
         if(eviarOrdenDireccion ){
           console.log("Entro aqui 1");
           var pedidocabecera = {
           "pedido": pedido,
           "detalles": data,
           "tiposolicitudentrega": tiposEntrega
         }

         console.log(pedido);
         console.log(data);
         console.log(pedidocabecera);

         this.categoriaFactory
         .InsertPedidoPanoramax(pedidocabecera)
         .subscribe((resultInsertPedidoPanoramax: any) => {

           for(var i=0; i<data.length; i++){
             data[i].SolicitudExameneId = resultInsertPedidoPanoramax.Id;
           }

           this.categoriaFactory
           .InsertDetallePedidoPanoramax(data)
           .subscribe((resultInsertDetallePedidoPanoramax: any) => {
             console.log("Resultado - InsertDetallePedidoPanoramax: ",resultInsertDetallePedidoPanoramax);

             var data2 = [];

             this.tiposEntrega.filter((item) => {
               if (item.selected == true) {
                 data2.push({
                   SolicitudExameneId: resultInsertPedidoPanoramax.Id,
                   TipoEntregaResultadoId: item.Id
                 })
               }
               return null;
             });

             this.tipoEntregaResultadosFactory
             .InsertarSolicitudTipoEntrega(data2)
             .subscribe((resultInsertarSolicitudTipoEntrega: any) => {
               console.log("Resultado - InsertarSolicitudTipoEntrega: ",resultInsertarSolicitudTipoEntrega);


               this.loadingCtrl.create({
                 spinner: 'hide',
                 content: "Su orden se ha realizado satisfactoriamente.",
                 duration: 2000
               }).present();

               this.nav = this.app.getActiveNavs();
               this.nav[0].setRoot(HomePage);

               //this.app.getRootNav().popToRoot();


             })
             err => {
               console.log("Error - InsertarSolicitudTipoEntrega", err);
               this.mensajeEnToast('Error: ' + err);
             }
           })
           err => {
             console.log("Error - InsertDetallePedidoPanoramax", err);
             this.mensajeEnToast('Error: ' + err);
           }
         })
         err => {
           console.log("Error - InsertPedidoPanoramax", err);
           this.mensajeEnToast('Error: ' + err);
         }
         }else{
           this.mensajeEnToast('Por favor seleccione una dirección de envio.');
         }

     }
     else{
       this.mensajeEnToast('Por favor seleccione una de las formas de entrega.');
     }
   }

   mensajeEnToast( mensaje:string ){
     this.toastCtrl.create({
         message: mensaje,
         duration: 2000,
         showCloseButton: true,
         closeButtonText: 'Ok'
       })
       .present();
   }

   cancelarOrden(){
     this.app.getRootNav().popToRoot();
   }
 }
