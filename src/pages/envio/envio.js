var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TipoEntregaResultadosFactory } from '../../providers/TipoEntregaResultadosFactory';
import * as rootScope from '../../app/rootScope';
import { SignUpFactory } from '../../providers/SignUpFactory';
import { CategoriaFactory } from '../../providers/CategoriaFactory';
var $rootScope = rootScope.default;
/**
 * Generated class for the EnvioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EnvioPage = /** @class */ (function () {
    function EnvioPage(navCtrl, navParams, tipoEntregaResultadosFactory, signUpFactory, categoriaFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tipoEntregaResultadosFactory = tipoEntregaResultadosFactory;
        this.signUpFactory = signUpFactory;
        this.categoriaFactory = categoriaFactory;
        //this.tiposEntrega = [];
        this.usr = $rootScope.user;
        this.paciente = this.navCtrl.parent.pacienteId;
        this.profileData = $rootScope.user;
        this.direccionesId = null;
        this.bucarTiposEntrega();
        this.cargarDirecciones();
        this.observaciones = "";
        this.direccionPedido = null;
        this.direcciones = [];
        console.log("Pacienteeeeeeeeeeeee", this.paciente);
    }
    EnvioPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EnvioPage');
    };
    EnvioPage.prototype.bucarTiposEntrega = function () {
        var _this = this;
        this.tipoEntregaResultadosFactory
            .GetAllTipoEntregaResultados()
            .subscribe(function (result) {
            _this.tiposEntrega = result.rows;
            console.log("Resultado ", _this.tiposEntrega);
        });
        (function (err) { return console.log("error", err); });
    };
    EnvioPage.prototype.CheckAddress = function (direccionPedido) {
        console.log("Entro aquiiiiiiiiiiiiiiii");
        this.direccionPedido = direccionPedido;
        console.log("direccionesssssssssss", this.direccionPedido);
        if (this.direccionPedido === 'nva') {
            var direccion = {};
            this.navCtrl.push('DireccionNuevaPage', {
                IdDirecciones: this.direccionesId
            });
            console.log("Dir Nueva", this.direccionesId);
        }
        ;
    };
    ;
    EnvioPage.prototype.cargarDirecciones = function () {
        var _this = this;
        this.signUpFactory
            .LoadAddress(this.profileData.Id)
            .subscribe(function (result) {
            _this.direcciones = result;
            console.log("------direcciones-----");
            console.log(_this.direcciones);
            console.log("------direcciones-----");
        });
        (function (err) { return console.log("error", err); });
    };
    EnvioPage.prototype.EnviarPedidoPanoramax = function () {
        var _this = this;
        var data = this.navCtrl.parent.pedido;
        var pedido = {
            "DoctorNombre": this.usr.nombres + " " + this.usr.apellidos,
            "DoctorId": this.usr.Id,
            "PacienteId": this.paciente.Id,
            "PacienteNombre": this.paciente.nombres + " " + this.paciente.apellidos,
            "observaciones": this.observaciones,
            "TipoServicioId": this.navCtrl.parent.categoriaId,
            "TipoEstadoId": 1,
            "TipoServicioNombre": (this.navCtrl.parent.categoriaId = 1 ? "" : ""),
            "EmpresaId": this.direccionPedido
        };
        var pedidocabecera = {
            "pedido": pedido,
            "detalles": data
        };
        console.log(pedido);
        console.log(data);
        console.log(pedidocabecera);
        this.categoriaFactory
            .InsertPedidoPanoramax(pedidocabecera)
            .subscribe(function (result) {
            _this.categoriaFactory
                .InsertDetallePedidoPanoramax(data)
                .subscribe(function (result) {
                console.log("Envio resultado: ", result);
            });
            (function (err) { return console.log("error", err); });
        });
        (function (err) { return console.log("error", err); });
    };
    EnvioPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-envio',
            templateUrl: 'envio.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            TipoEntregaResultadosFactory,
            SignUpFactory,
            CategoriaFactory])
    ], EnvioPage);
    return EnvioPage;
}());
export { EnvioPage };
//# sourceMappingURL=envio.js.map