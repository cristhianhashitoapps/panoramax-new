var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/Rx';
import { Slides } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';
import { Toast } from '@ionic-native/toast';
import { Component, ViewChild } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import * as rootScope from '../../app/rootScope';
var $rootScope = rootScope.default;
/**
* Generated class for the SignUp page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
var SignUp = /** @class */ (function () {
    function SignUp(navCtrl, navParams, menu, signUpFactory, toast, auth, loadingCtrl, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menu = menu;
        this.signUpFactory = signUpFactory;
        this.toast = toast;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.menu.close();
        this.menu.swipeEnable(false);
        this.usr = {};
        this.login = {};
        this.empresa = {};
    }
    SignUp.prototype.MostrarRegistro = function (esProveedor) {
        this.usr = {
            Proveedor: esProveedor
        };
        this.slides.slideTo(2, 500);
    };
    ;
    SignUp.prototype.MostrarInicioSesion = function () {
        this.slides.slideTo(0, 500);
    };
    ;
    SignUp.prototype.MostrarInicio = function () {
        this.slides.slideTo(1, 500);
    };
    ;
    SignUp.prototype.SignUp = function () {
        var _this = this;
        console.log("HOLA !!!!!!!!!!!");
        this.loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: "Espera un momento<br>estamos guardando tu información... ",
            duration: 3000
        });
        this.loading.present();
        this.usr.Empresa = this.empresa;
        if (this.usr.Terminos) {
            this.signUpFactory
                .SignUp(this.usr)
                .subscribe(function (result) {
                $rootScope.user = result;
                console.log(result);
                console.log("user" + $rootScope.user.clave);
                //$state.go('validar');
                _this.navCtrl.push('ValidatorPage', {});
            }),
                function (err) { return console.log("error", err); };
        }
        else {
            console.log('Debe de aceptar los terminos antes de continuar!!!');
        }
    };
    ;
    SignUp.prototype.Login = function () {
        var _this = this;
        this.loading = this.loadingCtrl.create({
            /*spinner: 'hide',*/
            content: "Espera un momento<br>estamos guardando tu información... "
            /*duration: 3000*/
        });
        console.log(this.usr);
        this.auth
            .login(this.usr)
            .map(function (res) { return res.json(); })
            .subscribe(function (result) {
            console.log(result);
            _this.auth.setToken(result.Token);
            _this.loading.dismiss();
            $rootScope.user = _this.auth.getPayload().sub;
            _this.events.publish('user:logued', _this.auth.getPayload().sub);
            _this.navCtrl.setRoot('HomePage');
            if (_this.usr.ISFACEBOOK) {
                _this.navCtrl.push('PerfilPage');
            }
        }, function (error) {
            console.log(error);
            _this.loading.dismiss();
            _this.toast.show('Se presento un error al iniciar sesión :' + error._body, '2500', 'bottom').subscribe(function (toast) {
                console.log(toast);
            });
            ;
        });
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], SignUp.prototype, "slides", void 0);
    SignUp = __decorate([
        IonicPage({ name: 'SignUp' }),
        Component({
            selector: 'page-signup',
            templateUrl: 'signup.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            MenuController,
            SignUpFactory,
            Toast,
            AuthService,
            LoadingController,
            Events])
    ], SignUp);
    return SignUp;
}());
export { SignUp };
//# sourceMappingURL=signup.js.map