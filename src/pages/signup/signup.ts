import 'rxjs/Rx';
import { Slides } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';
import { ToastController } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';
import { Loading, LoadingController } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

import * as rootScope from '../../app/rootScope';

import { FCM } from '@ionic-native/fcm';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { UserFactory } from '../../providers/UserFactory';


const $rootScope = rootScope.default;

/**
* Generated class for the SignUp page.
*
* See http://ionicframework.com/docs/components/#navigation for more info
* on Ionic pages and navigation.
*/
@IonicPage({ name: 'SignUp' })
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignUp {
  @ViewChild(Slides) slides: Slides;
  public usr;
  public login;
  public empresa;
  public loading: Loading;
  public fbResponse: any;
  public estado;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    public signUpFactory: SignUpFactory,
    public auth: AuthService,
    public loadingCtrl: LoadingController,
    public events: Events,
    public toastCtrl: ToastController,
    private fcm: FCM,
    private localNotifications: LocalNotifications,
    private userFactory: UserFactory
    ) {

    this.menu.close();
    this.menu.swipeEnable(false);
    this.usr = {};
    this.login ={}; 
    this.empresa = {};

    

    

  }

  MostrarRegistro (esProveedor) {
    this.usr = {
      Proveedor: esProveedor
    };
    this.slides.slideTo(2, 500);
  };

  MostrarInicioSesion () {
    this.slides.slideTo(0, 500);
  };

  MostrarInicio () {
    this.slides.slideTo(1, 500);
  };

  SignUp() {

    if (this.usr.Terminos) {

      this.loadingCtrl.create({
        spinner: 'hide',
        content: "Espera un momento<br>estamos guardando tu información... ",
        duration: 3000
      }).present();

      this.usr.Empresa = this.empresa;

      this.signUpFactory
      .SignUp(this.usr)
      .subscribe((result: any) => {

        //almacenar el token del dispositivo
        this.fcm.getToken().then(token=>{
          let userWithToken = result;
          userWithToken.DevicePushId = token;
          this.signUpFactory.Update(userWithToken.Id,userWithToken)
          .subscribe((res:any)=>{
             console.log("se guardo bien la notificacion");
          },
          err=>{
              console.log("no se guardo"+err);
          })

        })
        //fin token del dispositivo

        $rootScope.user = result;
        console.log("SignUp - Result: " + result);
        console.log("SignUp - User: "+$rootScope.user.clave);
          //$state.go('validar');
          this.navCtrl.push('ValidatorPage', {});
        }),
      err => {
        console.log("SignUp Error: ", err);

        this.toastCtrl.create({
          message: 'Error: ' + err._body,
          duration: 2000,
          showCloseButton: true,
          closeButtonText: 'Ok'
        })
        .present();
      }
    } 
    else {
      this.toastCtrl.create({
        message: 'Debe de aceptar los terminos antes de continuar.',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
    }
  };

  Login () {
    this.loading = this.loadingCtrl.create({
      /*spinner: 'hide',*/
      content: "Espera un momento<br>estamos guardando tu información... "
      /*duration: 3000*/
    });

    console.log(this.usr)
    this.auth
    .login(this.usr)
    .map(res => res.json())
    .subscribe((result: any) => {
      console.log("Login - Result: " + result);
      this.auth.setToken(result.Token);
      this.loading.dismiss(); 

      $rootScope.user = this.auth.getPayload().sub;
      this.events.publish('user:logued', this.auth.getPayload().sub);
      this.navCtrl.setRoot('HomePage');

      if(this.usr.ISFACEBOOK) {
        this.navCtrl.push('PerfilPage');
      }
    },
    error => {
      console.log("--//-- SignUp - Login --//--");
      console.log(error);
      console.log("--//-------------------//--");

      this.loading.dismiss();

      this.toastCtrl.create({
        message: 'Se presento un error al iniciar sesión: '+error._body,
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
    });
  }
} 