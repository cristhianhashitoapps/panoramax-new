import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoriaFactory } from '../../providers/CategoriaFactory';

/**
* Generated class for the RadiologiaTomografiaPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
 selector: 'page-radiologia-tomografia',
 templateUrl: 'radiologia-tomografia.html',
})
export class RadiologiaTomografiaPage {

 private categorias;
 private checkboxes;
 private pedido;
 private pedidoId;

 constructor(
   public navCtrl: NavController,
   public navParams: NavParams,
   public categoriasFactory: CategoriaFactory,
   private app: App,
   public toastCtrl: ToastController) {

   this.pedido = [];
   this.pedidoId = [];
   this.checkboxes = [];

   this.categoriasFactory
   .GetCategoriasByTipoServicioId(2)
   .subscribe(
     res => {
       this.categorias = res.rows;

     },
     err => {
       console.log('error: ', err);
     }
     );
 }

 ionViewDidLoad() {
   console.log('ionViewDidLoad RadiologiaTomografiaPage');
 }

 gestionItemPedido( categoriaId:number, subcategoriaId:number ){

   if(subcategoriaId == null){

     let categoria = this.categorias.filter((item) => {
       if (item.Id == categoriaId) {
         return item;
       }
       return null;
     })[0];

     for(let subCategoria of categoria.SubCategoria){

       var item = {
         "campoAbierto" : "ninguno",
         "SubCategoriumId": subCategoria.Id,
         "SubCategoriumNombre":subCategoria.nombre,
         "CategoriaId":subCategoria.CategoriumId,
         "CategoriaNombre":categoria.nombre,
         "SolicitudExameneId": "",
         "SubCategoriumClase":subCategoria.clase
       };

       if(!this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == true){
         console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
         this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==64||subCategoria.Id==65||subCategoria.Id==66||subCategoria.Id==67||subCategoria.Id==94||subCategoria.Id==105||subCategoria.Id==106?this.checkboxes[categoria+''+subCategoria]:true);
         this.pedido.push(item);
         this.pedidoId.push(subCategoria.Id);
       }
       else{
         if(this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == false){
           this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==64||subCategoria.Id==65||subCategoria.Id==66||subCategoria.Id==67||subCategoria.Id==94||subCategoria.Id==105||subCategoria.Id==106?this.checkboxes[categoria+''+subCategoria]:false);
           this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
           this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
         }
         else{

         }
       }
     }

   }
   else{
     let categoria = this.categorias.filter((item) => {
       if (item.Id == categoriaId) {
         return item;
       }
       return null;
     })[0];

     let subCategoria = categoria.SubCategoria.filter((item) => {
       if (item.Id == subcategoriaId) {
         return item;
       }
       return null;
     })[0];


     var item = {
       "campoAbierto" : "ninguno",
       "SubCategoriumId": subCategoria.Id,
       "SubCategoriumNombre":subCategoria.nombre,
       "CategoriaId":subCategoria.CategoriumId,
       "CategoriaNombre":categoria.nombre,
       "SolicitudExameneId": "",
       "SubCategoriumClase":subCategoria.clase
     };

     if(this.pedidoId.includes(subCategoria.Id)){
       console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre + " this.checkboxes[categoria.Id +''+ subCategoria.Id]: " + this.checkboxes[categoria.Id +''+ subCategoria.Id] );
       this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==64||subCategoria.Id==65||subCategoria.Id==66||subCategoria.Id==67||subCategoria.Id==94||subCategoria.Id==105||subCategoria.Id==106?this.checkboxes[categoria+''+subCategoria]:false);
       this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
       this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
     }
     else{
       console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
       this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==64||subCategoria.Id==65||subCategoria.Id==66||subCategoria.Id==67||subCategoria.Id==94||subCategoria.Id==105||subCategoria.Id==106?this.checkboxes[categoria+''+subCategoria]:true);
       this.pedido.push(item);
       this.pedidoId.push(subCategoria.Id);
     }
   }

  console.log("--//---//-- RadiologiaTomografiaPage --//---//---");
  for(let item of this.pedido){ console.log(item); }
    console.log("--//---//----//-----//---//---//--");
}

actualizarCampo( categoria:number, subCategoria:number ){

if(this.pedido[this.pedidoId.indexOf(subCategoria)] == undefined){
   this.gestionItemPedido( categoria, subCategoria);
}

this.pedido[this.pedidoId.indexOf(subCategoria)].campoAbierto  = this.checkboxes[categoria+''+subCategoria];


console.log("--//---//-- TomografiaFotografiaPage  --//---//---");
for(let item of this.pedido){ console.log(item); }
  console.log("--//---//----//-----//---//---//--");

}
/*
actualizarCampo( categoria:number, subCategoria:number ){

  this.pedido[this.pedidoId.indexOf(subCategoria)].campoAbierto = this.checkboxes[categoria+''+subCategoria];

  console.log("--//---//-- RadiologiaTomografiaPage  --//---//---");
  for(let item of this.pedido){ console.log(item); }
    console.log("--//---//----//-----//---//---//--");

}
*/
switchTabs(){
  if(this.pedido.length == 0){

    this.toastCtrl.create({
      message: 'Por favor seleccione uno de los paquetes preestablecidos',
      duration: 2000,
      showCloseButton: true,
      closeButtonText: 'Ok'
    })
    .present();

  }
  else{
    this.navCtrl.parent.pedido = this.pedido;
    this.navCtrl.parent.pedidoId = this.pedidoId;
    //Ir a paciente
    this.navCtrl.parent.getByIndex(1).enabled = false;
    this.navCtrl.parent.getByIndex(3).enabled = true;
    this.navCtrl.parent.select(3);
  }
}

cancelarOrden(){
  this.app.getRootNav().popToRoot();
}
}
