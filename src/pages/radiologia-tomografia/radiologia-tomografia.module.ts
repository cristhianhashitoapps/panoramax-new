import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RadiologiaTomografiaPage } from './radiologia-tomografia';

@NgModule({
  declarations: [
    RadiologiaTomografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(RadiologiaTomografiaPage),
  ],
})
export class RadiologiaTomografiaPageModule {}
