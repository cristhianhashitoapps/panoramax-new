import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
import * as rootScope                 from '../../app/rootScope';
const $rootScope = rootScope.default;


/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pedidos',
  templateUrl: 'pedidos.html',
})
export class PedidosPage {
	public pedidos;
	public usr;
	public doctor;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams, 
  	public signUpFactory: SignUpFactory) {

  	 this.pedidos = [];
  	 this.doctor = {};
  	 this.usr = $rootScope.user;
  	 this.BuscarPedido();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PedidosPage');
  }

  BuscarPedido(){
console.log("Usuario",this.usr);
 	this.doctor = $rootScope.user;
 	console.log("Usuario doctor",this.doctor);
  	this.signUpFactory
	    .GetOrdersPanoramax(this.doctor.Id)
	    .subscribe((result: any) => {
	      this.pedidos = result;
	      console.log("Pedidosssssssssss",this.pedidos);
	      
	    }),
	err => console.log("error", err);
  }

  

   VerDetalle(Id) {
    this.navCtrl.push('DetallePedidosPage', {Id: Id});
  }

  CambiarEstado(id){

    console.log("Pedido",id);
  }

}
