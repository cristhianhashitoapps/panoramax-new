var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import * as rootScope from '../../app/rootScope';
var $rootScope = rootScope.default;
/**
 * Generated class for the PedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PedidosPage = /** @class */ (function () {
    function PedidosPage(navCtrl, navParams, signUpFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signUpFactory = signUpFactory;
        this.pedidos = [];
        this.doctor = {};
        this.usr = $rootScope.user;
        this.BuscarPedido();
    }
    PedidosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PedidosPage');
    };
    PedidosPage.prototype.BuscarPedido = function () {
        var _this = this;
        console.log("Usuario", this.usr);
        this.doctor = $rootScope.user;
        console.log("Usuario doctor", this.doctor);
        this.signUpFactory
            .GetOrdersPanoramax(this.doctor.Id)
            .subscribe(function (result) {
            _this.pedidos = result;
            console.log("Pedidosssssssssss", _this.pedidos);
        }),
            function (err) { return console.log("error", err); };
    };
    PedidosPage.prototype.VerDetalle = function (Id) {
        this.navCtrl.push('DetallePedidosPage', { Id: Id });
    };
    PedidosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-pedidos',
            templateUrl: 'pedidos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            SignUpFactory])
    ], PedidosPage);
    return PedidosPage;
}());
export { PedidosPage };
//# sourceMappingURL=pedidos.js.map