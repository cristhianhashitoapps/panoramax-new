import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TomografiaPage } from './tomografia';

@NgModule({
  declarations: [
    TomografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(TomografiaPage),
  ],
})
export class TomografiaPageModule {}
