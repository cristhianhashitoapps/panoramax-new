var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaFactory } from '../../providers/CategoriaFactory';
/**
 * Generated class for the TomografiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TomografiaPage = /** @class */ (function () {
    function TomografiaPage(navCtrl, navParams, categoriasFactory) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categoriasFactory = categoriasFactory;
        this.categoriasFactory
            .GetCategoriasByTipoServicioId(1)
            .subscribe(function (res) {
            _this.categorias = res.rows;
        }, function (err) {
            console.log('error: ', err);
        });
    }
    TomografiaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TomografiaPage');
    };
    TomografiaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tomografia',
            templateUrl: 'tomografia.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, CategoriaFactory])
    ], TomografiaPage);
    return TomografiaPage;
}());
export { TomografiaPage };
//# sourceMappingURL=tomografia.js.map