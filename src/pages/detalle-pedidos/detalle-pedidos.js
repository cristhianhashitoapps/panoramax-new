var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
/**
 * Generated class for the DetallePedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DetallePedidosPage = /** @class */ (function () {
    function DetallePedidosPage(navCtrl, navParams, signUpFactory) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signUpFactory = signUpFactory;
        var detalles = [];
        console.log("Parametro", this.navParams.get('Id'));
        this.signUpFactory
            .CargarDetallesPorSolicitudExamen(this.navParams.get('Id').Id)
            .subscribe(function (result) {
            console.log("entro aqui", result);
            var data = result.map(function (item) {
                var result = item.SubCategorium;
                result.textocategoria = item.campoAbierto || '';
                result.textocategoria = result.textocategoria != 'ninguno' ? result.textocategoria : '';
                result.categorianombre = result.Categorium.nombre;
            }).sort(function (a, b) {
                return a.CategoriumId - b.CategoriumId;
            });
            var clase = null;
            var showClase = null;
            var uniqueCategory = result
                .map(function (item) {
                console.log("Entro aquiwwwwwwwwwwwwwwwwwwwwwww", item.SubCategorium.CategoriumId);
                return item.SubCategorium.CategoriumId;
            })
                .filter(function (value, index, self) {
                return self.indexOf(value) === index;
            })
                .forEach(function (item) {
                var subCategoryItems = result.filter(function (sub) {
                    return sub.SubCategorium.CategoriumId == item;
                }).sort(function (a, b) {
                    return a.Id - b.Id;
                }).forEach(function (sub, index) {
                    console.log("Entro aqui", index);
                    console.log("Entro aqui", sub);
                    if (index == 0) {
                        console.log("Entro aquissssssssssssssssssss");
                        detalles.push({
                            name: sub.SubCategorium.Categorium.nombre,
                            image: sub.ImagenPrincipal,
                            subs: []
                        });
                    }
                    showClase = clase != sub.SubCategorium.Categorium.clase;
                    if (clase != sub.SubCategorium.Categorium.clase) {
                        clase = sub.SubCategorium.Categorium.clase;
                    }
                    detalles[detalles.length - 1].subs.push({
                        name: sub.SubCategorium.nombre,
                        clase: showClase ? sub.SubCategorium.clase : null,
                        CssClass: sub.SubCategorium.CssClass,
                        text: sub.SubCategorium.textocategoria
                    });
                });
            });
            console.log("Detalles final", detalles);
            _this.detallesFinal = detalles;
        });
    }
    DetallePedidosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetallePedidosPage');
    };
    DetallePedidosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-detalle-pedidos',
            templateUrl: 'detalle-pedidos.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            SignUpFactory])
    ], DetallePedidosPage);
    return DetallePedidosPage;
}());
export { DetallePedidosPage };
//# sourceMappingURL=detalle-pedidos.js.map