import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory}                 from '../../providers/SignUpFactory';


/**
 * Generated class for the DetallePedidosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
  selector: 'page-detalle-pedidos',
  templateUrl: 'detalle-pedidos.html',
})
 export class DetallePedidosPage {
   public detallesFinal:any;
   public modal;
   public observacion;
   public mediosEntrega;

   constructor(
     public navCtrl: NavController, 
     public navParams: NavParams,
     public signUpFactory: SignUpFactory) {

     let detalles = [];
     console.log("Parametro",this.navParams.get('Id'));
     this.observacion = this.navParams.get('Id').observaciones;
      console.log("observacion", this.observacion);
      this.mediosEntrega = this.navParams.get('Id').SolicitudTipoEntregas;
      console.log("mediosEntrega", this.mediosEntrega);

     

     this.signUpFactory
     .CargarDetallesPorSolicitudExamen(this.navParams.get('Id').Id)
     .subscribe(result => { 
      console.log("entro aqui",result);
      var data = result.map(function(item) {
        var result = item.SubCategorium;

        result.textocategoria  = item.campoAbierto || '';
        result.textocategoria  = result.textocategoria != 'ninguno' ? result.textocategoria : '';
        result.categorianombre = result.Categorium.nombre;

      }).sort(function(a, b) {
        return a.CategoriumId - b.CategoriumId;
      });

      var clase = null;
      var showClase = null;
      var uniqueCategory = result
      .map(function(item) {
       console.log("Entro aquiwwwwwwwwwwwwwwwwwwwwwww",item.SubCategorium.CategoriumId);
       return item.SubCategorium.CategoriumId;

     })
      .filter(function(value, index, self) {
        return self.indexOf(value) === index;
      })
      .forEach(function(item) {
        var subCategoryItems = result.filter(function(sub) {
          return sub.SubCategorium.CategoriumId == item;

        }).sort(function(a, b) {
          return a.Id - b.Id;

        }).forEach(function(sub, index) {
         console.log("Entro aqui",index);
         console.log("Entro aqui",sub);
         if (index == 0) {

          console.log("Entro aquissssssssssssssssssss");

          detalles.push({
            name: sub.SubCategorium.Categorium.nombre,
            image: sub.ImagenPrincipal,
            subs: []


          });
        }


        showClase = clase != sub.SubCategorium.Categorium.clase;
        if (clase != sub.SubCategorium.Categorium.clase) {

          clase = sub.SubCategorium.Categorium.clase;
        }



        detalles[detalles.length - 1].subs.push({
          Id: sub.SubCategorium.Id,
          name: sub.SubCategorium.nombre,
          //clase: showClase ? sub.SubCategorium.clase : null,
          clase: sub.SubCategorium.clase,
          CssClass: sub.SubCategorium.CssClass,
          text: sub.SubCategorium.textocategoria
        });
      });
      });

      detalles[detalles.length - 1].subs.sort(function(a,b) {return (a.Id > b.Id) ? 1 : ((b.Id > a.Id) ? -1 : 0);} ); 

      console.log("Detalles final",detalles);
      this.detallesFinal = detalles;

    });
   }

   ionViewDidLoad() {
    console.log('ionViewDidLoad DetallePedidosPage');
  }

}
