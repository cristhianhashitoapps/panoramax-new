import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoriaFactory } from '../../providers/CategoriaFactory';

/**
 * Generated class for the TomografiaFotografiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-tomografia-fotografia',
 	templateUrl: 'tomografia-fotografia.html',
 })
 export class TomografiaFotografiaPage {

 	private categorias;
 	private checkboxes;
 	private pedido;
 	private pedidoId;

 	private mostrarTomografia : boolean;
 	private mostrarFotografia : boolean;

 	constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public categoriasFactory: CategoriaFactory,
     private app: App,
     public toastCtrl: ToastController) {

 		console.log("TomografiaFotografiaPage - constructor");

 		this.pedido = [];
 		this.pedidoId = [];
 		this.checkboxes = [];

 		this.pedidoId = this.navCtrl.parent.pedidoId;
 		this.pedido = this.navCtrl.parent.pedido;

 		this.mostrarTomografia = this.navCtrl.parent.mostrarTomografia;
 		this.mostrarFotografia = this.navCtrl.parent.mostrarFotografia;

     console.log(this.mostrarTomografia +" : "+ this.mostrarFotografia);

 		/*
 		console.log("--//---//-- TomografiaFotografiaPage --//---//---");
 		for(let item of this.pedido){ console.log(item); }
 		console.log("--//---//----//-----//---//---//--");
   */
   this.categoriasFactory
   .GetCategoriasByTipoServicioId(1)
   .subscribe(
     res => {
       this.categorias = res.rows;
     },
     err => {
       console.log('error: ', err);
     }
     );
 }

 ionViewDidLoad() {
   console.log('ionViewDidLoad TomografiaFotografiaPage');
 }

 switchTabs(){
   if(this.pedido.length == 0){

     this.toastCtrl.create({
       message: 'Por favor seleccione uno de los paquetes preestablecidos',
       duration: 2000,
       showCloseButton: true,
       closeButtonText: 'Ok'
     })
     .present();

   }
   else{
     this.navCtrl.parent.pedido = this.pedido;
     this.navCtrl.parent.pedidoId = this.pedidoId;
     //Ir a paciente
     this.navCtrl.parent.getByIndex(2).enabled = false;
     this.navCtrl.parent.getByIndex(3).enabled = true;
     this.navCtrl.parent.select(3);
   }
 }

 gestionItemPedido( categoriaId:number, subcategoriaId:number ){

   if(subcategoriaId == null){

       let categoria = this.categorias.filter((item) => {
         if (item.Id == categoriaId) {
           return item;
         }
         return null;
       })[0];

       if((categoriaId == 6 || categoriaId == 8) && this.checkboxes['C' + categoriaId] == true){
         this.mostrarTomografia = true;
       }
       else{
         this.mostrarTomografia = false;
       }

       if(categoriaId == 9 && this.checkboxes['C' + categoriaId] == true){
         this.mostrarFotografia = true;
       }
       else{
         this.mostrarFotografia = false;
       }

       for(let subCategoria of categoria.SubCategoria){

         var item = {
           "campoAbierto" : "ninguno",
           "SubCategoriumId": subCategoria.Id,
           "SubCategoriumNombre":subCategoria.nombre,
           "CategoriaId":subCategoria.CategoriumId,
           "CategoriaNombre":categoria.nombre,
           "SolicitudExameneId": "",
           "SubCategoriumClase":subCategoria.clase
         };

         if(!this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == true){
           console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
           this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==49||subCategoria.Id==50?this.checkboxes[categoria+''+subCategoria]:true);
           this.pedido.push(item);
           this.pedidoId.push(subCategoria.Id);
         }
         else{
           if(this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == false){
             this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==49||subCategoria.Id==50?this.checkboxes[categoria+''+subCategoria]:false);
             this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
             this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
           }
         }
       }

     }
     else{
       let categoria = this.categorias.filter((item) => {
         if (item.Id == categoriaId) {
           return item;
         }
         return null;
       })[0];

       let subCategoria = categoria.SubCategoria.filter((item) => {
         if (item.Id == subcategoriaId) {
           return item;
         }
         return null;
       })[0];


       var item = {
         "campoAbierto" : "ninguno",
         "SubCategoriumId": subCategoria.Id,
         "SubCategoriumNombre":subCategoria.nombre,
         "CategoriaId":subCategoria.CategoriumId,
         "CategoriaNombre":categoria.nombre,
         "SolicitudExameneId": "",
         "SubCategoriumClase":subCategoria.clase
       };

       if(this.pedidoId.includes(subCategoria.Id)){
         console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre + " this.checkboxes[categoria.Id +''+ subCategoria.Id]: " + this.checkboxes[categoria.Id +''+ subCategoria.Id] );
         this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==49||subCategoria.Id==50?this.checkboxes[categoria+''+subCategoria]:false);
         this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
         this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
       }
       else{
         console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
         this.checkboxes[categoria.Id +''+ subCategoria.Id] = (subCategoria.Id==49||subCategoria.Id==50?this.checkboxes[categoria+''+subCategoria]:true);
         this.pedido.push(item);
         this.pedidoId.push(subCategoria.Id);
       }
     }

    console.log("--//-- OrtodonciaCirugiaPage --//--");
    for(let item of this.pedido){ console.log(item); }
      console.log("--//---------------------------//--");
  }

actualizarCampo( categoria:number, subCategoria:number ){

  if(this.pedido[this.pedidoId.indexOf(subCategoria)] == undefined){
     this.gestionItemPedido( categoria, subCategoria);
  }

  this.pedido[this.pedidoId.indexOf(subCategoria)].campoAbierto = this.checkboxes[categoria+''+subCategoria];


  console.log("--//---//-- TomografiaFotografiaPage	 --//---//---");
  for(let item of this.pedido){ console.log(item); }
    console.log("--//---//----//-----//---//---//--");

}

cancelarOrden(){
  this.app.getRootNav().popToRoot();
}

}
