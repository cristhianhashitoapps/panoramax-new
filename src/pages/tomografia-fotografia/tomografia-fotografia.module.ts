import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TomografiaFotografiaPage } from './tomografia-fotografia';

@NgModule({
  declarations: [
    TomografiaFotografiaPage,
  ],
  imports: [
    IonicPageModule.forChild(TomografiaFotografiaPage),
  ],
})
export class TomografiaFotografiaPageModule {}
