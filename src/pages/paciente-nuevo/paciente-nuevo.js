var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
/**
 * Generated class for the PacienteNuevoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PacienteNuevoPage = /** @class */ (function () {
    function PacienteNuevoPage(navCtrl, navParams, signUpFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signUpFactory = signUpFactory;
        this.paciente = {};
        this.pacienteId = null;
    }
    PacienteNuevoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PacienteNuevoPage');
    };
    PacienteNuevoPage.prototype.Guardar = function () {
        var _this = this;
        this.signUpFactory
            .InsertPaciente(this.paciente)
            .subscribe(function (result) {
            _this.paciente = result;
            console.log("Paciente resultado", result);
            _this.navCtrl.push('PacientePage', {
                IdDirecciones: _this.pacienteId
            });
        });
        (function (err) {
            console.log('error: ', err);
        });
    };
    ;
    PacienteNuevoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-paciente-nuevo',
            templateUrl: 'paciente-nuevo.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            SignUpFactory])
    ], PacienteNuevoPage);
    return PacienteNuevoPage;
}());
export { PacienteNuevoPage };
//# sourceMappingURL=paciente-nuevo.js.map