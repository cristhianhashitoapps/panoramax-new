import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacienteNuevoPage } from './paciente-nuevo';

@NgModule({
  declarations: [
    PacienteNuevoPage,
  ],
  imports: [
    IonicPageModule.forChild(PacienteNuevoPage),
  ],
})
export class PacienteNuevoPageModule {}
