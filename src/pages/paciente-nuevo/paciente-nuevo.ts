import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
/**
 * Generated class for the PacienteNuevoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paciente-nuevo',
  templateUrl: 'paciente-nuevo.html',
})
export class PacienteNuevoPage {
  public paciente;
  public pacienteId;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
     public toastCtrl: ToastController,
  	public signUpFactory: SignUpFactory) {

  	this.paciente = {};
    this.pacienteId = null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacienteNuevoPage');
  }

  Guardar(){
     this.signUpFactory
     .InsertPaciente(this.paciente)
     .subscribe((result: any) => {
       this.paciente = result;
       console.log("Paciente resultado",result);
        this.navCtrl.push('PacientePage', {
        IdDirecciones: this.pacienteId
      });
this.toastCtrl.create({
        message: 'Paciente agregado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
     })
     err => {
  			console.log('error: ', err);
  		}

  };

}
