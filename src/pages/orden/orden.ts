import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoriesPage } from '../categories/categories';
import { TomografiaFotografiaPage } from '../tomografia-fotografia/tomografia-fotografia';
import { PacientePage } from '../paciente/paciente';
import { EnvioPage } from '../envio/envio';

/**
 * Generated class for the OrdenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({ name: 'OrdenPage' })
@Component({
  selector: 'page-orden',
  templateUrl: 'orden.html',
})

export class OrdenPage {
	public pedido;
	public pedidoId;
	public categoriaId;
	public mostrarTomografia;
	public mostrarFotografia;

	public tabCategorias = CategoriesPage;
	public tabTomografiaFotografia = TomografiaFotografiaPage;
	public tabPaciente = PacientePage;
	public tabEnvio = EnvioPage;

	constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.pedido = [];
		this.pedidoId = [];

		this.mostrarTomografia = false;
		this.mostrarFotografia = false;
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad OrdenPage');
	}
}