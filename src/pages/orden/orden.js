var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriesPage } from '../categories/categories';
import { TomografiaFotografiaPage } from '../tomografia-fotografia/tomografia-fotografia';
import { PacientePage } from '../paciente/paciente';
import { EnvioPage } from '../envio/envio';
/**
 * Generated class for the OrdenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrdenPage = /** @class */ (function () {
    function OrdenPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tabCategorias = CategoriesPage;
        this.tabTomografiaFotografia = TomografiaFotografiaPage;
        this.tabPaciente = PacientePage;
        this.tabEnvio = EnvioPage;
        this.pedido = [];
        this.pedidoId = [];
        this.mostrarTomografia = false;
        this.mostrarFotografia = false;
    }
    OrdenPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrdenPage');
    };
    OrdenPage = __decorate([
        IonicPage({ name: 'OrdenPage' }),
        Component({
            selector: 'page-orden',
            templateUrl: 'orden.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], OrdenPage);
    return OrdenPage;
}());
export { OrdenPage };
//# sourceMappingURL=orden.js.map