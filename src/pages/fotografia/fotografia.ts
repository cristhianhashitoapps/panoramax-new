import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoriaFactory } from '../../providers/CategoriaFactory';

/**
 * Generated class for the FotografiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fotografia',
  templateUrl: 'fotografia.html',
})
export class FotografiaPage {

public categorias;

  constructor(public navCtrl: NavController, public navParams: NavParams, public categoriasFactory: CategoriaFactory) {

  this.categoriasFactory
  	.GetCategoriasByTipoServicioId(1)
  	.subscribe(
  		res => {
  			this.categorias = res.rows;
  		},
  		err => {
  			console.log('error: ', err);
  		}
 	);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FotografiaPage');
  }

}
