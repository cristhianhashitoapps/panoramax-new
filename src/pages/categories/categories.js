var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaFactory } from '../../providers/CategoriaFactory';
import { OrtodonciaCirugiaPage } from '../ortodoncia-cirugia/ortodoncia-cirugia';
import { RadiologiaTomografiaPage } from '../radiologia-tomografia/radiologia-tomografia';
/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CategoriesPage = /** @class */ (function () {
    function CategoriesPage(navCtrl, navParams, categoriasFactory, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categoriasFactory = categoriasFactory;
        this.app = app;
        this.categoriasFactory;
    }
    CategoriesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriesPage');
    };
    CategoriesPage.prototype.switchTabs = function (tipo) {
        if (tipo == 1) {
            this.navCtrl.parent.getByIndex(1).root = OrtodonciaCirugiaPage;
            this.navCtrl.parent.categoriaId = 1;
        }
        else if (tipo == 2) {
            this.navCtrl.parent.getByIndex(1).root = RadiologiaTomografiaPage;
            this.navCtrl.parent.categoriaId = 2;
        }
        this.navCtrl.parent.getByIndex(0).enabled = false;
        this.navCtrl.parent.getByIndex(1).enabled = true;
        this.navCtrl.parent.select(1);
    };
    CategoriesPage.prototype.cancelarOrden = function () {
        this.app.getRootNav().popToRoot();
    };
    CategoriesPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-categories',
            templateUrl: 'categories.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            CategoriaFactory,
            App])
    ], CategoriesPage);
    return CategoriesPage;
}());
export { CategoriesPage };
//# sourceMappingURL=categories.js.map