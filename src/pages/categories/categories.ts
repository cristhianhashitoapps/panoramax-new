import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategoriaFactory } from '../../providers/CategoriaFactory';

import { OrtodonciaCirugiaPage } from '../ortodoncia-cirugia/ortodoncia-cirugia';
import { RadiologiaTomografiaPage } from '../radiologia-tomografia/radiologia-tomografia';

/**
 * Generated class for the CategoriesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
  })

 export class CategoriesPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public categoriasFactory: CategoriaFactory,
    private app: App) {

    this.categoriasFactory;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriesPage');
  }

  switchTabs(tipo) {
    if(tipo == 1){
      this.navCtrl.parent.getByIndex(1).root = OrtodonciaCirugiaPage;
      this.navCtrl.parent.categoriaId = 1;
      this.navCtrl.parent.categoriaNombre="Estudios de Ortodoncia y Cirugía";
    }
    else if(tipo == 2){
      this.navCtrl.parent.getByIndex(1).root = RadiologiaTomografiaPage;
      this.navCtrl.parent.categoriaId = 2;
      this.navCtrl.parent.categoriaNombre="Radiología, Tomografía y otros servicios";
    }

    this.navCtrl.parent.getByIndex(0).enabled = false;
    this.navCtrl.parent.getByIndex(1).enabled = true;
    this.navCtrl.parent.select(1);
  }

  cancelarOrden(){
    this.app.getRootNav().popToRoot();
  }
}
