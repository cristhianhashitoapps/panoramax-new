var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import { FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the PacientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PacientePage = /** @class */ (function () {
    function PacientePage(navCtrl, navParams, signUpFactory, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signUpFactory = signUpFactory;
        this.toastCtrl = toastCtrl;
        this.searchControl = new FormControl();
        this.paciente = {};
        this.pacientes = [];
        this.pacienteLista = null;
        this.searchTerm = "";
        this.consultarPacientes();
    }
    PacientePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PacientePage');
        this.setFilteredItems();
        /*this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
    
               this.setFilteredItems();
    
           });*/
    };
    PacientePage.prototype.setFilteredItems = function () {
        var _this = this;
        this.signUpFactory
            .filterItems(this.searchTerm)
            .subscribe(function (result) {
            _this.pacientes = result.rows;
            _this.toastCtrl.create({
                message: 'Se realizo la consulta exitosamente.. Debe seleccionar el paciente',
                duration: 2000,
                showCloseButton: true,
                closeButtonText: 'Ok'
            })
                .present();
        });
        (function (err) {
            console.log('error: ', err);
        });
    };
    PacientePage.prototype.consultarPacientes = function () {
        var _this = this;
        this.signUpFactory
            .GetPaciente(this.paciente)
            .subscribe(function (result) {
            _this.pacientes = result.rows;
        });
        (function (err) {
            console.log('error: ', err);
        });
    };
    PacientePage.prototype.CheckAddress = function (pacienteLista) {
        this.pacienteLista = pacienteLista;
        this.paciente.Id = this.pacienteLista;
        if (this.pacienteLista === 'nva') {
            var direccion = {};
            this.navCtrl.push('PacienteNuevoPage', {});
        }
        ;
        this.buscarPaciente();
    };
    ;
    PacientePage.prototype.buscarPaciente = function () {
        var _this = this;
        this.signUpFactory
            .GetPacienteId(this.pacienteLista)
            .subscribe(function (result) {
            _this.paciente = result;
            console.log("Paciente xxxxxxxxxx", _this.paciente);
        });
    };
    PacientePage.prototype.Actualizar = function () {
        var _this = this;
        console.log("Paciente", this.paciente);
        if (this.paciente.method == 0) {
            this.toastCtrl.create({
                message: 'Se debe seleccionar almenos un paciente para actualizar',
                duration: 2000,
                showCloseButton: true,
                closeButtonText: 'Ok'
            })
                .present();
        }
        else {
            this.signUpFactory
                .UpdatePaciente(this.pacienteLista, this.paciente)
                .subscribe(function (result) {
                _this.paciente = result;
                _this.toastCtrl.create({
                    message: 'La información del paciente actualizada exitosamente',
                    duration: 2000,
                    showCloseButton: true,
                    closeButtonText: 'Ok'
                })
                    .present();
                console.log("Paciente resultado", result);
            });
            (function (err) {
                console.log('error: ', err);
            });
            this.consultarPacientes();
        }
    };
    ;
    PacientePage.prototype.Eliminar = function (Id) {
        var _this = this;
        this.signUpFactory
            .DeletePacienteId(Id)
            .subscribe(function (result) {
            _this.pacientes = result;
        });
        (function (err) {
            console.log('error: ', err);
        });
        //this.buscarPaciente();
        this.consultarPacientes();
    };
    PacientePage.prototype.limpiarFormulario = function () {
        this.paciente.nombre = "",
            this.paciente.apellido = "";
    };
    PacientePage.prototype.switchTabs = function () {
        console.log("Paciente limpiar ", this.paciente);
        if (this.paciente.method == 0) {
            console.log("Paciente limpiar ", this.paciente);
            this.toastCtrl.create({
                message: 'Por favor seleccione uno paciente para continuar',
                duration: 2000,
                showCloseButton: true,
                closeButtonText: 'Ok'
            })
                .present();
        }
        else {
            this.navCtrl.parent.getByIndex(3).enabled = false;
            this.navCtrl.parent.pacienteId = this.paciente;
            this.navCtrl.parent.getByIndex(4).enabled = true;
            this.navCtrl.parent.select(4);
        }
    };
    PacientePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-paciente',
            templateUrl: 'paciente.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            SignUpFactory,
            ToastController])
    ], PacientePage);
    return PacientePage;
}());
export { PacientePage };
//# sourceMappingURL=paciente.js.map