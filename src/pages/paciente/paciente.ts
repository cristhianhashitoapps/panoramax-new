import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
import { FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular';
import { App } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';

/**
 * Generated class for the PacientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */



@IonicPage()
@Component({
  selector: 'page-paciente',
  templateUrl: 'paciente.html',
})
export class PacientePage {
	public paciente;
  public pacienteLista;
  public pacientes;
  public IdPaciente;
  public searchTerm;
  public searchControl;
  public pacienteId;
  public pacienteGurdado;


  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams, 
    private app: App,
    public actionSheetCtrl: ActionSheetController,
  	public signUpFactory: SignUpFactory,
    public toastCtrl: ToastController) {

     this.searchControl = new FormControl();

  	this.paciente = {};
    this.pacientes = [];
    this.pacienteLista = null;
    this.pacienteId = null;
    this.searchTerm = "";
    this.pacienteGurdado = false;
   // this.consultarPacientes();

   
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PacientePage');
    // this.setFilteredItems();
     /*this.searchControl.valueChanges.debounceTime(700).subscribe(search => {
 
            this.setFilteredItems();
 
        });*/
  } 

  Guardar(){
    if(this.paciente.nombres == "" || this.paciente.apellidos == "" || this.paciente.telefono == null
      || this.paciente.edad == null || this.paciente.email == null || this.paciente.cedula == null || this.paciente.fechaNacimiento == null){
      this.toastCtrl.create({
        message: 'Informacion del paciente incompleta',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();

    }else{
      this.signUpFactory
     .InsertPaciente(this.paciente)
     .subscribe((result: any) => {
       this.paciente = result;
       console.log("Paciente resultado",result);
      this.toastCtrl.create({
        message: 'Paciente agregado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
     })
     err => {
        console.log('error: ', err);
      }
      this.pacienteGurdado = true;
    }
  };
 
    /*setFilteredItems() {

     this.signUpFactory
    .filterItems(this.searchTerm)
     .subscribe((result: any) => {
       this.pacientes = result.rows;
        let actionSheet = this.pacientes;
         actionSheet.present();
        this.toastCtrl.create({
          message: 'Se realizo la consulta exitosamente.. Debe seleccionar un paciente',
          duration: 2000,
          showCloseButton: true,
          closeButtonText: 'Ok'
        })
        .present();
  
     })
     err => {
        console.log('error: ', err);
      }
 
    }

  consultarPacientes(){
     this.signUpFactory
    .GetPaciente(this.paciente)
     .subscribe((result: any) => {
       this.pacientes = result.rows;
       console.log(this.pacientes);
     })
     err => {
        console.log('error: ', err);
      }
  }

  CheckAddress(pacienteLista) {
    this.pacienteLista = pacienteLista;
    this.paciente.Id = this.pacienteLista

    if (this.pacienteLista === 'nva') {
      var direccion = {};
       this.navCtrl.push('PacienteNuevoPage', {
      });
    };
    this.buscarPaciente();
  };

  buscarPaciente(){
    this.signUpFactory
    .GetPacienteId(this.pacienteLista)
    .subscribe((result: any) => {
       this.paciente = result;
     })
  }


   Actualizar(){
   	console.log("Paciente", this.paciente);
      if(this.paciente.method == 0){

         this.toastCtrl.create({
        message: 'Se debe ingresar almenos un paciente para actualizar',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
    }else{
      this.signUpFactory
     .UpdatePaciente(this.pacienteLista,this.paciente)
     .subscribe((result: any) => {
       this.paciente = result;
       this.toastCtrl.create({
        message: 'La información del paciente actualizada exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
       console.log("Paciente resultado",result);
     })
     err => {
        console.log('error: ', err);
      }
      this.consultarPacientes();
    }
     

  };

  Eliminar(Id){

 console.log("Paciente xxxxxxxxxx",Id);

   if(Id == undefined){

         this.toastCtrl.create({
        message: 'Se debe ingresar almenos un paciente para eliminar',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
    }else{
       this.signUpFactory
     .DeletePacienteId(Id)
     .subscribe((result: any) => {
       this.pacientes = result;


     })
     err => {
        console.log('error: ', err);
      }

      this.toastCtrl.create({
        message: 'El paciente fue eliminado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
       this.limpiarFormulario();
      this.buscarPaciente();
      
 
     }

  }


  limpiarFormulario(){
    console.log("Nombre",this.paciente.nombres);
    this.paciente.nombres = "",
    this.paciente.apellidos ="";
    this.paciente.telefono = null;
    this.paciente.edad = null;
    this.paciente.email = "";
    this.paciente.cedula = null;
    this.paciente.fechaNacimiento = null;
    this.pacientes = [];


    console.log("Nombre borrado",this.paciente.nombres)
    } */



  switchTabs(){

 console.log("Paciente limpiar ", this.paciente);
     if(this.pacienteGurdado){

       this.navCtrl.parent.getByIndex(3).enabled = false;
          this.navCtrl.parent.pacienteId = this.paciente;
          this.navCtrl.parent.getByIndex(4).enabled = true;

          this.navCtrl.parent.select(4);

    }else{
            this.toastCtrl.create({
        message: 'Por favor seleccione un paciente para continuar',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
    }

         
   } 

  cancelarOrden(){
    console.log("entro qui");
    this.app.getRootNav().popToRoot();
  }

}
