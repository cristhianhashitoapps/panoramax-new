import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PacientePage } from './paciente';

import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

@NgModule({
  declarations: [
    PacientePage,
  ],
  imports: [
    IonicPageModule.forChild(PacientePage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PacientePageModule {}
