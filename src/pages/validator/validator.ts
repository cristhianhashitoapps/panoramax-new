import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AuthService }                from 'ng2-ui-auth';

import * as rootScope                 from '../../app/rootScope';

/**
 * Generated class for the ValidatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 const $rootScope = rootScope.default;

 @IonicPage()
 @Component({
 	selector: 'page-validator',
 	templateUrl: 'validator.html',
 })
 export class ValidatorPage {
 	public usr;
 	public loginData;

 	constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService) {
 		this.usr = {};
 		this.loginData ={}; 
 	}

 	ionViewDidLoad() {
 		console.log('ionViewDidLoad ValidatorPage');
 	}

 	Validar(){

 		console.log("From user input: " + this.loginData.clave);

 		//$rootScope.loginData.telefono = $rootScope.user.telefono;
 		//this.loginData.clave = $rootScope.user.clave;
 		this.loginData.telefono = $rootScope.user.telefono;

/*
      $rootScope.doValidate = function() {
        $rootScope.loginData.clave = $rootScope.loginData.clave.toUpperCase();

        this.auth
          .login($rootScope.loginData)
          .then(function(result) {
            //$state.go('app.index');


          })
          .catch(function(error) {
            console.log(error);
          });
      };
      */


      this.auth
      .login(this.loginData)
      .map(res => res.json())
      .flatMap((result: any) => {
      	console.log("HOLA:", result);
      	this.auth.setToken(result.Token);



      	$rootScope.user = this.auth.getPayload().sub;
      	//this.events.publish('user:logued', this.auth.getPayload().sub);
      	//return this.auth.login(this.usr);
      	return this.auth.login(this.loginData);
      })
      .subscribe(result => {
      	//this.loading.dismiss();
      	this.navCtrl.setRoot('HomePage');
      },
      error => {



      	let errorjson = JSON.parse(error._body);  

      	console.log(errorjson.name);

      });
  }

}
