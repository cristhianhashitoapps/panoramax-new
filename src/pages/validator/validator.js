var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';
import * as rootScope from '../../app/rootScope';
/**
 * Generated class for the ValidatorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var $rootScope = rootScope.default;
var ValidatorPage = /** @class */ (function () {
    function ValidatorPage(navCtrl, navParams, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.usr = {};
        this.loginData = {};
    }
    ValidatorPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ValidatorPage');
    };
    ValidatorPage.prototype.Validar = function () {
        var _this = this;
        console.log("From user input: " + this.loginData.clave);
        //$rootScope.loginData.telefono = $rootScope.user.telefono;
        //this.loginData.clave = $rootScope.user.clave;
        this.loginData.telefono = $rootScope.user.telefono;
        /*
              $rootScope.doValidate = function() {
                $rootScope.loginData.clave = $rootScope.loginData.clave.toUpperCase();
        
                this.auth
                  .login($rootScope.loginData)
                  .then(function(result) {
                    //$state.go('app.index');
        
        
                  })
                  .catch(function(error) {
                    console.log(error);
                  });
              };
              */
        this.auth
            .login(this.loginData)
            .map(function (res) { return res.json(); })
            .flatMap(function (result) {
            console.log("HOLA:", result);
            _this.auth.setToken(result.Token);
            $rootScope.user = _this.auth.getPayload().sub;
            //this.events.publish('user:logued', this.auth.getPayload().sub);
            //return this.auth.login(this.usr);
            return _this.auth.login(_this.loginData);
        })
            .subscribe(function (result) {
            //this.loading.dismiss();
            _this.navCtrl.setRoot('HomePage');
        }, function (error) {
            var errorjson = JSON.parse(error._body);
            console.log(errorjson.name);
        });
    };
    ValidatorPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-validator',
            templateUrl: 'validator.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AuthService])
    ], ValidatorPage);
    return ValidatorPage;
}());
export { ValidatorPage };
//# sourceMappingURL=validator.js.map