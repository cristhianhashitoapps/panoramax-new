import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Toast }       from '@ionic-native/toast';
import { AuthService }                from 'ng2-ui-auth';
import * as rootScope                 from '../../app/rootScope';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
import { ToastController } from 'ionic-angular';

const $rootScope = rootScope.default;

/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',

})
export class PerfilPage {
	public direccionesId;
  public usr;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
     public auth: AuthService,
  	private toast: Toast,
    public toastCtrl: ToastController,
    public signUpFactory: SignUpFactory) {

  	this.direccionesId = null;
    this.usr = $rootScope.user;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerfilPage');
  }

  Buscar () {
    console.log("direccionesId", this.direccionesId);
       this.navCtrl.push('DireccionPage', {
        IdDirecciones: this.direccionesId
      });
   
  }

  Actualizar (usuario) {

    console.log("Perfil editar" , usuario.telefono)

    this.signUpFactory
    .Update(usuario.Id,usuario)
    .subscribe((result: any) => {
       console.log("resultado editado",result);
      this.usr = result;
      $rootScope.user = this.usr;
      console.log("ususa", $rootScope.user);
       this.toastCtrl.create({
        message: 'Perfil actualizado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
        }),
        err => console.log("error", err);

  }

}
