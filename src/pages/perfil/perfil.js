var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { AuthService } from 'ng2-ui-auth';
import * as rootScope from '../../app/rootScope';
import { SignUpFactory } from '../../providers/SignUpFactory';
var $rootScope = rootScope.default;
/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, auth, toast, signUpFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.toast = toast;
        this.signUpFactory = signUpFactory;
        this.direccionesId = null;
        this.usr = $rootScope.user;
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.Buscar = function () {
        console.log("direccionesId", this.direccionesId);
        this.navCtrl.push('DireccionPage', {
            IdDirecciones: this.direccionesId
        });
    };
    PerfilPage.prototype.Actualizar = function (usuario) {
        var _this = this;
        console.log("Perfil editar", usuario.telefono);
        this.signUpFactory
            .Update(usuario.Id, usuario)
            .subscribe(function (result) {
            console.log("resultado editado", result);
            _this.usr = result;
            $rootScope.user = _this.usr;
            console.log("ususa", $rootScope.user);
        }),
            function (err) { return console.log("error", err); };
    };
    PerfilPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-perfil',
            templateUrl: 'perfil.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AuthService,
            Toast,
            SignUpFactory])
    ], PerfilPage);
    return PerfilPage;
}());
export { PerfilPage };
//# sourceMappingURL=perfil.js.map