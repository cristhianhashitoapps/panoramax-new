import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BannerFactory } from '../../providers/BannerFactory';

import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-home',
 	templateUrl: 'home.html',
 })
 export class HomePage {

 	@ViewChild(Slides) slides: Slides;

 	public banners;

 	constructor(public navCtrl: NavController, public navParams: NavParams, public bannerFactory: BannerFactory) {

 		this.bannerFactory
 		.CargarBanners()
 		.subscribe(
 			res => {
 				this.banners = res.data;
 				//console.log('banners: ', res);
 			},
 			err => {
 				console.log('error: ', err);
 			});
 	}

 	ionViewDidLoad() {
 		console.log('ionViewDidLoad HomePage');
 	}

 	goToSlide() {
 		this.slides.slideTo(2, 500);
 	}

 	slideChanged() {
 		let currentIndex = this.slides.getActiveIndex();
 		console.log('Current index is', currentIndex);
 	}

 	crearNuevaOrden(){
 		this.navCtrl.push('OrdenPage', {});
 	}

 	consultarOrdenes(){
 		this.navCtrl.push('PedidosPage', {});
 	}
 }
