var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpFactory } from '../../providers/SignUpFactory';
import * as rootScope from '../../app/rootScope';
var $rootScope = rootScope.default;
/**
 * Generated class for the DiereccionNuevaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DireccionNuevaPage = /** @class */ (function () {
    function DireccionNuevaPage(navCtrl, navParams, signUpFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.signUpFactory = signUpFactory;
        this.direccion = {};
        this.direcciones = [];
        this.direccionesId = null;
    }
    DireccionNuevaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DireccionNuevaPage');
    };
    DireccionNuevaPage.prototype.Guardar = function () {
        var _this = this;
        this.direccion.DoctorId = $rootScope.user.Id;
        this.signUpFactory
            .InsertAddress(this.direccion)
            .subscribe(function (result) {
            _this.direcciones = result;
            console.log("Direccion resultado", result);
            _this.navCtrl.push('EnvioPage', {
                direcciones: _this.direccionesId
            });
            console.log("Direccion nuevaaaaaa", _this.direccionesId);
        });
        console.log("Direccion", this.direccionesId);
    };
    ;
    DireccionNuevaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-diereccion-nueva',
            templateUrl: 'direccion-nueva.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            SignUpFactory])
    ], DireccionNuevaPage);
    return DireccionNuevaPage;
}());
export { DireccionNuevaPage };
//# sourceMappingURL=direccion-nueva.js.map