import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
import * as rootScope                 from '../../app/rootScope';


const $rootScope = rootScope.default;

/**
 * Generated class for the DiereccionNuevaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-diereccion-nueva',
  templateUrl: 'direccion-nueva.html',
})
export class DireccionNuevaPage {
	public direccion;
  public usr;
  public direcciones;
  public direccionesId;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams,
    public toastCtrl: ToastController,
  	public signUpFactory: SignUpFactory) {

  	this.direccion = {};
    this.direcciones =[];
    this.direccionesId = null;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DireccionNuevaPage');
  }

  Guardar(){
     this.direccion.DoctorId = $rootScope.user.Id;
     this.signUpFactory
     .InsertAddress(this.direccion)
     .subscribe((result: any) => {
       this.direcciones = result;
       console.log("Direccion resultado",result);
        this.navCtrl.push('EnvioPage', {
        direcciones: this.direccionesId
      });
        this.toastCtrl.create({
        message: 'Perfil actualizado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
        console.log("Direccion nuevaaaaaa",this.direccionesId);
     })
  	console.log("Direccion",this.direccionesId);


  };

}
