import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DireccionNuevaPage } from './direccion-nueva';

@NgModule({
  declarations: [
    DireccionNuevaPage,
  ],
  imports: [
    IonicPageModule.forChild(DireccionNuevaPage),
  ],
})
export class DireccionNuevaPageModule {}
