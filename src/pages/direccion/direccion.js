var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from 'ng2-ui-auth';
import * as rootScope from '../../app/rootScope';
import { SignUpFactory } from '../../providers/SignUpFactory';
var $rootScope = rootScope.default;
/**
 * Generated class for the DireccionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DireccionPage = /** @class */ (function () {
    function DireccionPage(navCtrl, navParams, auth, signUpFactory) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.signUpFactory = signUpFactory;
        this.direccionesId = null;
        this.subcategorias = [];
        this.direcciones = [];
        this.empresa = {};
        this.direccionesId = null;
        this.usr = $rootScope.user;
        console.log("usuario", this.usr);
        this.SignAdres();
    }
    DireccionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DireccionPage');
    };
    DireccionPage.prototype.Buscar = function () {
        console.log("direccionesId", this.direccionesId);
        this.navCtrl.push('DireccionNuevaPage', {
            IdDirecciones: this.direccionesId
        });
    };
    DireccionPage.prototype.SignAdres = function () {
        var _this = this;
        console.log("HOLA !!!!!!!!!!!");
        this.signUpFactory
            .LoadAddress(this.usr.Id)
            .subscribe(function (result) {
            _this.direcciones = result;
            console.log(result);
        }),
            function (err) { return console.log("error", err); };
    };
    ;
    DireccionPage.prototype.Editar = function (Id) {
        var _this = this;
        this.signUpFactory
            .UpdateAddress(Id.Id, Id)
            .subscribe(function (result) {
            console.log("HOLA !!!!!!!!!!!", result);
            _this.direcciones = result;
        }),
            function (err) { return console.log("error", err); };
    };
    DireccionPage.prototype.Eliminar = function (id) {
        var _this = this;
        console.log("HOLA !!!!!!!!!!!", id);
        this.signUpFactory
            .DeleteAddress(id)
            .subscribe(function (result) {
            console.log("HOLA !!!!!!!!!!!", result);
            _this.direcciones = result;
        }),
            function (err) { return console.log("error", err); };
    };
    DireccionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-direccion',
            templateUrl: 'direccion.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            AuthService,
            SignUpFactory])
    ], DireccionPage);
    return DireccionPage;
}());
export { DireccionPage };
//# sourceMappingURL=direccion.js.map