import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService }                from 'ng2-ui-auth';
import * as rootScope                 from '../../app/rootScope';
import { SignUpFactory}                 from '../../providers/SignUpFactory';
import { ToastController } from 'ionic-angular';


const $rootScope = rootScope.default;

/**
 * Generated class for the DireccionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-direccion',
  templateUrl: 'direccion.html',
})
export class DireccionPage {
	public direccionesId;
  public direcciones;
	public subcategorias;
	public usr;
	public empresa;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public auth: AuthService,
     public toastCtrl: ToastController,
  	public signUpFactory: SignUpFactory) {
  	this.direccionesId = null;
  	this.subcategorias  = [];
    this.direcciones = [];
 	  this.empresa = {};
  	this.direccionesId = null;
    this.usr = $rootScope.user;

    console.log("usuario",this.usr);
    this.SignAdres();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DireccionPage');
  }

  Buscar () {
    console.log("direccionesId", this.direccionesId);
       this.navCtrl.push('DireccionNuevaPage', {
        IdDirecciones: this.direccionesId
      });
   
  }

  SignAdres() {
    console.log("HOLA !!!!!!!!!!!")

          this.signUpFactory
            .LoadAddress(this.usr.Id)
            .subscribe((result: any) => {
              this.direcciones = result;
              console.log(result);
              
            }),
            err => console.log("error", err);
         
  };

  Editar(Id){
    this.signUpFactory
    .UpdateAddress(Id.Id, Id)
    .subscribe((result: any) => {
      console.log("HOLA !!!!!!!!!!!",result)
              this.direcciones = result;
 this.toastCtrl.create({
        message: 'Perfil actualizado exitosamente',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();
            }),
    err => console.log("error", err);
  }

Eliminar(id){
console.log("HOLA !!!!!!!!!!!",id)
   this.signUpFactory
   .DeleteAddress(id)
   .subscribe((result: any) => {
      console.log("HOLA !!!!!!!!!!!",result)
              this.direcciones = result;
            }),
    err => console.log("error", err);
}
  

}
