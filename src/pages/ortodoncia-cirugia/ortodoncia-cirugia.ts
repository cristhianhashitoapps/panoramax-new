import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaFactory } from '../../providers/CategoriaFactory';


/**
 * Generated class for the OrtodonciaCirugiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage({ name: 'OrtodonciaCirugiaPage' })
 @Component({
   selector: 'page-ortodoncia-cirugia',
   templateUrl: 'ortodoncia-cirugia.html',
 })
 export class OrtodonciaCirugiaPage {

   private categorias;
   private checkboxes;
   private pedido;
   private pedidoId;

   private mostrarTomografia : boolean;
   private mostrarFotografia : boolean;

   constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public categoriasFactory: CategoriaFactory,
     private app: App,
     public toastCtrl: ToastController) {

     this.pedido = [];
     this.pedidoId = [];
     this.checkboxes = [];

     this.mostrarFotografia = false;
     this.mostrarTomografia = false;

     this.categoriasFactory
     .GetCategoriasByTipoServicioId(1)
     .subscribe(
       res => {
         this.categorias = res.rows;
       },
       err => {
         console.log('error: ', err);
       }
       );
   }

   ionViewDidLoad() {
     console.log('ionViewDidLoad OrtodonciaCirugiaPage');
   }

   gestionItemPedido( categoriaId:number, subcategoriaId:number ){

     if(subcategoriaId == null){

       let categoria = this.categorias.filter((item) => {
         if (item.Id == categoriaId) {
           return item;
         }
         return null;
       })[0];

       if(categoriaId == 6 || categoriaId == 8){
          if(this.checkboxes['C' + categoriaId] == true){
            this.mostrarTomografia = true;
          }
          else{
           this.mostrarTomografia = false;
         }
       }
       

       if(categoriaId == 9){
         if(this.checkboxes['C' + categoriaId] == true){
           this.mostrarFotografia = true;
         }
         else{
           this.mostrarFotografia = false;
         }
       }
       

       for(let subCategoria of categoria.SubCategoria){

         var item = {
           "campoAbierto" : "ninguno",
           "SubCategoriumId": subCategoria.Id,
           "SubCategoriumNombre":subCategoria.nombre,
           "CategoriaId":subCategoria.CategoriumId,
           "CategoriaNombre":categoria.nombre,
           "SolicitudExameneId": ""
         };

         if(!this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == true){
           console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
           this.checkboxes[categoria.Id +''+ subCategoria.Id] = true;
           this.pedido.push(item);
           this.pedidoId.push(subCategoria.Id);
         }
         else{ 
           if(this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == false){
             this.checkboxes[categoria.Id +''+ subCategoria.Id] = false;            
             this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
             this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
           }
         }
       }

     }
     else{
       let categoria = this.categorias.filter((item) => {
         if (item.Id == categoriaId) {
           return item;
         }
         return null;
       })[0];

       let subCategoria = categoria.SubCategoria.filter((item) => {
         if (item.Id == subcategoriaId) {
           return item;
         }
         return null;
       })[0];


       var item = {
         "campoAbierto" : "ninguno",
         "SubCategoriumId": subCategoria.Id,
         "SubCategoriumNombre":subCategoria.nombre,
         "CategoriaId":subCategoria.CategoriumId,
         "CategoriaNombre":categoria.nombre,
         "SolicitudExameneId": ""
       };

       if(this.pedidoId.includes(subCategoria.Id)){
         console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre + " this.checkboxes[categoria.Id +''+ subCategoria.Id]: " + this.checkboxes[categoria.Id +''+ subCategoria.Id] );
         this.checkboxes[categoria.Id +''+ subCategoria.Id] = false;            
         this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id),1);
         this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id),1);
       }
       else{
         console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
         this.checkboxes[categoria.Id +''+ subCategoria.Id] = true;
         this.pedido.push(item);
         this.pedidoId.push(subCategoria.Id);
       }
     }

    console.log("--//-- OrtodonciaCirugiaPage --//--");
    for(let item of this.pedido){ console.log(item); }
      console.log("--//---------------------------//--");
  }

  switchTabs(){

    if(this.pedido.length == 0){

      this.toastCtrl.create({
        message: 'Por favor seleccione uno de los paquetes preestablecidos',
        duration: 2000,
        showCloseButton: true,
        closeButtonText: 'Ok'
      })
      .present();

    }
    else{
      this.navCtrl.parent.pedido = this.pedido;
      this.navCtrl.parent.pedidoId = this.pedidoId;
      this.navCtrl.parent.mostrarFotografia = this.mostrarFotografia;
      this.navCtrl.parent.mostrarTomografia = this.mostrarTomografia;

      if(this.mostrarTomografia || this.mostrarFotografia){
        //Ir a Tomografia-Fotografia
        this.navCtrl.parent.getByIndex(1).enabled = false;
        this.navCtrl.parent.getByIndex(2).enabled = true;
        this.navCtrl.parent.select(2, {pedido: this.pedido});
      }
      else{
        //Ir a paciente
        this.navCtrl.parent.getByIndex(1).enabled = false;
        this.navCtrl.parent.getByIndex(3).enabled = true;
        this.navCtrl.parent.select(3);
      } 
    }       
  }

  cancelarOrden(){
    this.app.getRootNav().popToRoot();
  }
}