import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrtodonciaCirugiaPage } from './ortodoncia-cirugia';

@NgModule({
  declarations: [
    OrtodonciaCirugiaPage,
  ],
  imports: [
    IonicPageModule.forChild(OrtodonciaCirugiaPage),
  ],
})
export class OrtodonciaCirugiaPageModule {}
