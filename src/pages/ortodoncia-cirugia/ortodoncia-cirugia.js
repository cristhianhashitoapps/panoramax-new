var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { App } from 'ionic-angular';
import { Component } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaFactory } from '../../providers/CategoriaFactory';
/**
 * Generated class for the OrtodonciaCirugiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrtodonciaCirugiaPage = /** @class */ (function () {
    function OrtodonciaCirugiaPage(navCtrl, navParams, categoriasFactory, app, toastCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categoriasFactory = categoriasFactory;
        this.app = app;
        this.toastCtrl = toastCtrl;
        this.pedido = [];
        this.pedidoId = [];
        this.checkboxes = [];
        this.mostrarFotografia = false;
        this.mostrarTomografia = false;
        this.categoriasFactory
            .GetCategoriasByTipoServicioId(1)
            .subscribe(function (res) {
            _this.categorias = res.rows;
        }, function (err) {
            console.log('error: ', err);
        });
    }
    OrtodonciaCirugiaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrtodonciaCirugiaPage');
    };
    OrtodonciaCirugiaPage.prototype.gestionItemPedido = function (id, nombre) {
        for (var _i = 0, _a = this.categorias; _i < _a.length; _i++) {
            var categoria = _a[_i];
            /*
              Carga y elimina una categoria completa.
              */
            if (id == categoria.Id) {
                if ((id == 6 || id == 8) && this.checkboxes['C' + id] == true) {
                    this.mostrarTomografia = true;
                }
                else {
                    this.mostrarTomografia = false;
                }
                if (id == 9 && this.checkboxes['C' + id] == true) {
                    this.mostrarFotografia = true;
                }
                else {
                    this.mostrarFotografia = false;
                }
                for (var _b = 0, _c = categoria.SubCategoria; _b < _c.length; _b++) {
                    var subCategoria = _c[_b];
                    var item = {
                        "campoAbierto": "ninguno",
                        "SubCategoriumId": subCategoria.Id,
                        "SubCategoriumNombre": subCategoria.nombre,
                        "CategoriaId": subCategoria.CategoriumId,
                        "CategoriaNombre": categoria.nombre,
                        "SolicitudExameneId": ""
                    };
                    if (!this.pedidoId.includes(subCategoria.Id) && this.checkboxes['C' + categoria.Id] == true) {
                        console.log("Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre);
                        this.checkboxes[categoria.Id + '' + subCategoria.Id] = true;
                        this.pedido.push(item);
                        this.pedidoId.push(subCategoria.Id);
                    }
                    else {
                        this.checkboxes[categoria.Id + '' + subCategoria.Id] = false;
                        this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id), 1);
                        this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id), 1);
                    }
                }
                break;
            }
            else {
                for (var _d = 0, _e = categoria.SubCategoria; _d < _e.length; _d++) {
                    var subCategoria = _e[_d];
                    if (id == subCategoria.Id) {
                        var item = {
                            "campoAbierto": "ninguno",
                            "SubCategoriumId": subCategoria.Id,
                            "SubCategoriumNombre": subCategoria.nombre,
                            "CategoriaId": subCategoria.CategoriumId,
                            "CategoriaNombre": categoria.nombre,
                            "SolicitudExameneId": ""
                        };
                        if (this.pedidoId.includes(subCategoria.Id)) {
                            console.log("Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre + " this.checkboxes[categoria.Id +''+ subCategoria.Id]: " + this.checkboxes[categoria.Id + '' + subCategoria.Id]);
                            this.checkboxes[categoria.Id + '' + subCategoria.Id] = false;
                            this.pedido.splice(this.pedidoId.indexOf(subCategoria.Id), 1);
                            this.pedidoId.splice(this.pedidoId.indexOf(subCategoria.Id), 1);
                        }
                        /*else{
                          console.log( "Id: " + subCategoria.Id + " nombre: " + subCategoria.nombre );
                          this.checkboxes[categoria.Id +''+ subCategoria.Id] = true;
                          this.pedido.push(item);
                          this.pedidoId.push(subCategoria.Id);
                        }*/
                        break;
                    }
                }
            }
        }
        console.log("--//---//-- OrtodonciaCirugiaPage --//---//---");
        for (var _f = 0, _g = this.pedido; _f < _g.length; _f++) {
            var item_1 = _g[_f];
            console.log(item_1);
        }
        console.log("--//---//----//-----//---//---//--");
    };
    OrtodonciaCirugiaPage.prototype.switchTabs = function () {
        if (this.pedido.length == 0) {
            this.toastCtrl.create({
                message: 'Por favor seleccione uno de los paquetes preestablecidos',
                duration: 2000,
                showCloseButton: true,
                closeButtonText: 'Ok'
            })
                .present();
        }
        else {
            this.navCtrl.parent.pedido = this.pedido;
            this.navCtrl.parent.pedidoId = this.pedidoId;
            this.navCtrl.parent.mostrarFotografia = this.mostrarFotografia;
            this.navCtrl.parent.mostrarTomografia = this.mostrarTomografia;
            if (this.mostrarTomografia || this.mostrarFotografia) {
                //Ir a Tomografia-Fotografia
                this.navCtrl.parent.getByIndex(1).enabled = false;
                this.navCtrl.parent.getByIndex(2).enabled = true;
                this.navCtrl.parent.select(2, { pedido: this.pedido });
            }
            else {
                //Ir a paciente
                this.navCtrl.parent.getByIndex(1).enabled = false;
                this.navCtrl.parent.getByIndex(3).enabled = true;
                this.navCtrl.parent.select(3);
            }
        }
    };
    OrtodonciaCirugiaPage.prototype.cancelarOrden = function () {
        this.app.getRootNav().popToRoot();
    };
    OrtodonciaCirugiaPage = __decorate([
        IonicPage({ name: 'OrtodonciaCirugiaPage' }),
        Component({
            selector: 'page-ortodoncia-cirugia',
            templateUrl: 'ortodoncia-cirugia.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            CategoriaFactory,
            App,
            ToastController])
    ], OrtodonciaCirugiaPage);
    return OrtodonciaCirugiaPage;
}());
export { OrtodonciaCirugiaPage };
//# sourceMappingURL=ortodoncia-cirugia.js.map