var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
import { Transfer } from '@ionic-native/transfer';
//import { File } from '@ionic-native/file';
/*
  Generated class for the PerfilFactoryProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
    */
var UserFactory = /** @class */ (function () {
    function UserFactory(http, transfer /*, private file: File*/) {
        this.http = http;
        this.transfer = transfer; /*, private file: File*/
        this.urlBase = Config.ApiUrl;
        console.log('Hello UserFactory Provider');
        this.fileTransfer = this.transfer.create();
    }
    UserFactory.prototype.Guardar = function (Id, usr) {
        return this.http.put([this.urlBase, 'usuarios', Id].join('/'), usr).map(function (res) { return res.json(); });
    };
    ;
    UserFactory.prototype.GuardarAvatar = function (usr, filePath) {
        console.log('AvatarfilePath', filePath);
        var options = {
            fileKey: 'file',
            fileName: 'avatar.png',
            httpMethod: 'POST',
            mimeType: 'image/png',
            params: usr /*,
          chunkedMode: '',
          headers: '',*/
        };
        console.log(this.fileTransfer);
        console.log([
            this.urlBase,
            'usuarios',
            usr.Id,
            'nuevoavatar'
        ].join('/'));
        return this.fileTransfer.upload(filePath, [
            this.urlBase,
            'usuarios',
            usr.Id,
            'nuevoavatar'
        ].join('/'), options);
    };
    ;
    UserFactory.prototype.CargarBanners = function () {
        return this.http.get([this.urlBase, 'ImagenesBanner'].join('/')).map(function (res) { return res.json(); });
    };
    UserFactory.prototype.CargarUsuario = function (Id) {
        return this.http.get([this.urlBase, 'usuarios', Id].join('/')).map(function (res) { return res.json(); });
    };
    ;
    UserFactory.prototype.UrlAvatar = function (user) {
        return [this.urlBase.replace('/api/v00', ''), 'avatars', user.Id + '.png'].join('/');
    };
    ;
    UserFactory.prototype.GoToFacebook = function (access_token) {
        return this.http.post([this.urlBase, 'usuarios', 'facebook'].join('/'), access_token).map(function (res) { return res.json(); });
    };
    ;
    UserFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp, Transfer /*, private file: File*/])
    ], UserFactory);
    return UserFactory;
}());
export { UserFactory };
//# sourceMappingURL=UserFactory.js.map