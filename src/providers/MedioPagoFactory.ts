import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the MedioPagoFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class MedioPagoFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello MedioPagoFactory Provider');
  }

  GetAllMedioPago() {
    //return $http.get([urlBase, 'medios-pago'].join('/'));
    return this.http
      .get([this.urlBase, 'medios-pago'].join('/'))
      .map(res => res.json());
  }
}
