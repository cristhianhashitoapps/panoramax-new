import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the ParametroFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ParametroFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello DepartamentosFactory Provider');
  }

  GetValorDomicilio() {
    //return $http.get([urlBase, 'parametros', 1].join('/'));
    return this.http
      .get([this.urlBase, 'parametros', 1].join('/'))
      .map(res => res.json());
  }
}
