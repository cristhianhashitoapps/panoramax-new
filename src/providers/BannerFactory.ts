import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Config } from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the BannerFactory provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BannerFactory {

	public urlBase: string = Config.ApiUrl;

  constructor(public http: Http) {
    console.log('Hello BannerProvider Provider');
  }

  CargarBanners () {
    return this.http.get([this.urlBase, 'Banners' ].join('/')).map(res => res.json());
  }

}
