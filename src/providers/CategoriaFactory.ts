import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
//import 'rxjs/add/operator/map';

/*
  Generated class for the CategoriaFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CategoriaFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello CategoriaFactory Provider');
  }

  GetCategoriasByTipoServicioId(tiposervicio_id) {
    //return $http.get([urlBase, 'categorias',tiposervicio_id,'bytiposervicioid'].join('/'));
    return this.http
      .get([this.urlBase, 'categorias', tiposervicio_id, 'bytiposervicioid'].join('/'))
      .map(res => res.json());
  }

  GetTipoServicio() {
    //return $http.get([urlBase, 'tiposervicio'].join('/'));
    return this.http
      .get([this.urlBase, 'tiposervicio'].join('/'))
      .map(res => res.json());
  }

  //inserta el pedido de panoramax
  InsertPedidoPanoramax(pedido) {
    //return $http.post([urlBase, 'SolicitudExamenes'].join('/'), pedido);
    return this.http
      .post([this.urlBase, 'SolicitudExamenes'].join('/'), pedido)
      .map(res => res.json());
  }

  //inserta el detalle de cada item de panoramax
  InsertDetallePedidoPanoramax(detalle) {
    //return $http.post([urlBase, 'DetallesExamenes','InsertarDetallePedido'].join('/'), detalle);
    return this.http
      .post([this.urlBase, 'DetallesExamenes', 'InsertarDetallePedido'].join('/'), detalle)
      .map(res => res.json());
  }
}