var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
/*
  Generated class for the ProductFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var ProductFactory = /** @class */ (function () {
    function ProductFactory(http) {
        this.http = http;
        this.urlBase = Config.ApiUrl;
        console.log('Hello ProductFactory Provider');
    }
    ProductFactory.prototype.GetProducts = function () {
        //return $http.get([urlBase, 'productos'].join('/'));
        return this.http
            .get([this.urlBase, 'productos'].join('/'))
            .map(function (res) { return res.json(); });
    };
    ;
    ProductFactory.prototype.GetProductsByVariedad = function () {
        //return $http.get([urlBase, 'variedades'].join('/'));
        return this.http
            .get([this.urlBase, 'variedades'].join('/'))
            .map(function (res) { return res.json(); });
    };
    ProductFactory.prototype.InsertPedido = function (pedido) {
        //return $http.post([urlBase, 'pedidos'].join('/'), pedido);
        return this.http
            .post([this.urlBase, 'pedidos'].join('/'), pedido)
            .map(function (res) { return res.json(); });
    };
    ProductFactory.prototype.UpdatePedido = function (Id, pedido) {
        //return $http.put([urlBase, 'pedidos', Id].join('/'), pedido);
        return this.http
            .put([this.urlBase, 'pedidos', Id].join('/'), pedido)
            .map(function (res) { return res.json(); });
    };
    ;
    ProductFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp])
    ], ProductFactory);
    return ProductFactory;
}());
export { ProductFactory };
//# sourceMappingURL=ProductFactory.js.map