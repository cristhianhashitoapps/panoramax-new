var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
/*
  Generated class for the TipoEntregaResultadosFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var TipoEntregaResultadosFactory = /** @class */ (function () {
    function TipoEntregaResultadosFactory(http) {
        this.http = http;
        this.urlBase = Config.ApiUrl;
        console.log('Hello TipoEntregaResultadosFactory Provider');
    }
    TipoEntregaResultadosFactory.prototype.GetAllTipoEntregaResultados = function () {
        //return $http.get([urlBase, 'TipoEntregaResultados'].join('/'));
        return this.http
            .get([this.urlBase, 'TipoEntregaResultados'].join('/'))
            .map(function (res) { return res.json(); });
    };
    ;
    TipoEntregaResultadosFactory.prototype.InsertarSolicitudTipoEntrega = function (data) {
        //return $http.post([urlBase, 'SolicitudTipoEntrega'].join('/'), data);
        return this.http
            .get([this.urlBase, 'SolicitudTipoEntrega'].join('/'), data)
            .map(function (res) { return res.json(); });
    };
    TipoEntregaResultadosFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp])
    ], TipoEntregaResultadosFactory);
    return TipoEntregaResultadosFactory;
}());
export { TipoEntregaResultadosFactory };
//# sourceMappingURL=TipoEntregaResultadosFactory.js.map