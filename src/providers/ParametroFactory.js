var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
/*
  Generated class for the ParametroFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var ParametroFactory = /** @class */ (function () {
    function ParametroFactory(http) {
        this.http = http;
        this.urlBase = Config.ApiUrl;
        console.log('Hello DepartamentosFactory Provider');
    }
    ParametroFactory.prototype.GetValorDomicilio = function () {
        //return $http.get([urlBase, 'parametros', 1].join('/'));
        return this.http
            .get([this.urlBase, 'parametros', 1].join('/'))
            .map(function (res) { return res.json(); });
    };
    ParametroFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp])
    ], ParametroFactory);
    return ParametroFactory;
}());
export { ParametroFactory };
//# sourceMappingURL=ParametroFactory.js.map