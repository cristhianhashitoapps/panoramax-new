import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the ProductFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ProductFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello ProductFactory Provider');
  }

  GetProducts() {
    //return $http.get([urlBase, 'productos'].join('/'));
    return this.http
      .get([this.urlBase, 'productos'].join('/'))
      .map(res => res.json());
  };

  GetProductsByVariedad() {
    //return $http.get([urlBase, 'variedades'].join('/'));
    return this.http
      .get([this.urlBase, 'variedades'].join('/'))
      .map(res => res.json());
  }

  InsertPedido(pedido) {
    //return $http.post([urlBase, 'pedidos'].join('/'), pedido);
    return this.http
      .post([this.urlBase, 'pedidos'].join('/'), pedido)
      .map(res => res.json());
  }

  UpdatePedido(Id, pedido) {
    //return $http.put([urlBase, 'pedidos', Id].join('/'), pedido);
    return this.http
      .put([this.urlBase, 'pedidos', Id].join('/'), pedido)
      .map(res => res.json());
  };
}













