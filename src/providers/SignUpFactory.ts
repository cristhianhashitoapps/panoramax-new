  import { Injectable } from '@angular/core';
  import { JwtHttp } from 'ng2-ui-auth';
  import { Config } from './config';
  import 'rxjs/add/operator/map';

  /*
    Generated class for the SignUpFactory provider.

    See https://angular.io/docs/ts/latest/guide/dependency-injection.html
    for more info on providers and Angular DI.
      */
    @Injectable()
    export class SignUpFactory {
      urlBase: string = Config.ApiUrl;

      constructor(public http: JwtHttp) {
        console.log('Hello SignUpFactory Provider');
      }

      SignUp(usuario) {
        //return $http.post([urlBase, 'SignUp', ''].join('/'), usuario);
        return this.http
          .post([this.urlBase, 'SignUp', ''].join('/'), usuario)
          .map(res => res.json());
      }

      Update(telefono, usuario) {
        //return $http.put([urlBase,'Doctor',telefono].join('/'), usuario);
        return this.http
          .put([this.urlBase,'Doctor',telefono].join('/'), usuario)
          .map(res => res.json());
      }

      LoadAddress(telefono) {
        //return $http.get([urlBase,'Doctor',telefono,'Empresa'].join('/'));
        return this.http
          .get([this.urlBase,'Doctor',telefono,'Empresa'].join('/'))
          .map(res => res.json());
      }

      UpdateAddress(Id, direccion) {
        //return $http.put([urlBase,'Empresa',Id].join('/'), direccion);
        return this.http
          .put([this.urlBase,'Empresa',Id].join('/'), direccion)
          .map(res => res.json());
      }

      InsertAddress(direccion) {
        //return $http.post([urlBase,'Empresa'].join('/'), direccion);
        return this.http
          .post([this.urlBase,'Empresa'].join('/'), direccion)
          .map(res => res.json());
      }

      DeleteAddress(Id) {
        //return $http.delete([urlBase,'Empresa',Id].join('/'));
        return this.http
          .delete([this.urlBase,'Empresa',Id].join('/'))
          .map(res => res.json());
      }

      GetOrders(Telefono) {
        //return $http.get([urlBase,'usuarios',Telefono,'pedidos'].join('/'));
        return this.http
          .get([this.urlBase,'usuarios',Telefono,'pedidos'].join('/'))
          .map(res => res.json());
      }

      //obtener pedidos panoramax
      GetOrdersPanoramax(Id) {
        //return $http.get([urlBase,'Doctor',Id,'GetOrdersPanoramax'].join('/'));
        return this.http
          .get([this.urlBase,'Doctor',Id,'GetOrdersPanoramax'].join('/'))
          .map(res => res.json());
      }

      //inserta paciente panoramax
      InsertPaciente(paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .post([this.urlBase,'Paciente'].join('/'), paciente)
          .map(res => res.json());
      };
      //inserta paciente panoramax
      UpdatePaciente(Id,paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .put([this.urlBase,'Paciente',Id].join('/'), paciente)
          .map(res => res.json());
      };


      //inserta paciente panoramax
      GetPaciente(paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .get([this.urlBase,'Paciente'].join('/'), paciente)
          .map(res => res.json());
      };
      
      //inserta paciente panoramax
      GetPacienteId(Id) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .get([this.urlBase,'Paciente',Id].join('/'))
          .map(res => res.json());
      };


      //inserta paciente panoramax
      DeletePacienteId(Id) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .delete([this.urlBase,'Paciente',Id].join('/'))
          .map(res => res.json());
      };

     //inserta paciente panoramax
      UpdatePedido(Id,pedido) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
          .put([this.urlBase,'pedido',Id].join('/'), pedido)
          .map(res => res.json());
      };
      
      //inserta paciente panoramax
      CargarDetallesPorSolicitudExamen(Id) {
        //return $http.get([urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'), Id);
        return this.http
          .get([this.urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'))
          .map(res => res.json());
      }

      filterItems(searchTerm){
 
       return this.http
        .get([this.urlBase,'Paciente',searchTerm,'likename'].join('/'))
          .map(res => res.json());
   }

   getResults(nombre) {
        //return $http.get([urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'), Id);
        return this.http
          .get([this.urlBase,'Paciente',nombre,'likename'].join('/'))
          .map(res => res.json().filter(item => item.name.toLowerCase().startsWith(nombre.toLowerCase())));

      } 

      Validar(){
        
      }
    }