var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
//import 'rxjs/add/operator/map';
/*
  Generated class for the CategoriaFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var CategoriaFactory = /** @class */ (function () {
    function CategoriaFactory(http) {
        this.http = http;
        this.urlBase = Config.ApiUrl;
        console.log('Hello CategoriaFactory Provider');
    }
    CategoriaFactory.prototype.GetCategoriasByTipoServicioId = function (tiposervicio_id) {
        //return $http.get([urlBase, 'categorias',tiposervicio_id,'bytiposervicioid'].join('/'));
        return this.http
            .get([this.urlBase, 'categorias', tiposervicio_id, 'bytiposervicioid'].join('/'))
            .map(function (res) { return res.json(); });
    };
    CategoriaFactory.prototype.GetTipoServicio = function () {
        //return $http.get([urlBase, 'tiposervicio'].join('/'));
        return this.http
            .get([this.urlBase, 'tiposervicio'].join('/'))
            .map(function (res) { return res.json(); });
    };
    //inserta el pedido de panoramax
    CategoriaFactory.prototype.InsertPedidoPanoramax = function (pedido) {
        //return $http.post([urlBase, 'SolicitudExamenes'].join('/'), pedido);
        return this.http
            .post([this.urlBase, 'SolicitudExamenes'].join('/'), pedido)
            .map(function (res) { return res.json(); });
    };
    //inserta el detalle de cada item de panoramax
    CategoriaFactory.prototype.InsertDetallePedidoPanoramax = function (detalle) {
        //return $http.post([urlBase, 'DetallesExamenes','InsertarDetallePedido'].join('/'), detalle);
        return this.http
            .post([this.urlBase, 'DetallesExamenes', 'InsertarDetallePedido'].join('/'), detalle)
            .map(function (res) { return res.json(); });
    };
    CategoriaFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp])
    ], CategoriaFactory);
    return CategoriaFactory;
}());
export { CategoriaFactory };
//# sourceMappingURL=CategoriaFactory.js.map