import { Injectable }           from '@angular/core';
import { JwtHttp }              from 'ng2-ui-auth';
import { Config }               from './config';
import 'rxjs/add/operator/map';

/*
  Generated class for the TipoEntregaResultadosFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TipoEntregaResultadosFactory {
  urlBase: string = Config.ApiUrl;

  constructor(public http: JwtHttp) {
    console.log('Hello TipoEntregaResultadosFactory Provider');
  }

  GetAllTipoEntregaResultados() {
    //return $http.get([urlBase, 'TipoEntregaResultados'].join('/'));
    return this.http
      .get([this.urlBase, 'TipoEntregaResultados'].join('/'))
      .map(res => res.json());
  };

  InsertarSolicitudTipoEntrega(data) {
    //return $http.post([urlBase, 'SolicitudTipoEntrega'].join('/'), data);
    return this.http
      .post([this.urlBase, 'SolicitudTipoEntrega'].join('/'), data)
      .map(res => res.json());
  }
}
