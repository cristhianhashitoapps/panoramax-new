var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { JwtHttp } from 'ng2-ui-auth';
import { Config } from './config';
import 'rxjs/add/operator/map';
/*
  Generated class for the SignUpFactory provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
    */
var SignUpFactory = /** @class */ (function () {
    function SignUpFactory(http) {
        this.http = http;
        this.urlBase = Config.ApiUrl;
        console.log('Hello SignUpFactory Provider');
    }
    SignUpFactory.prototype.SignUp = function (usuario) {
        //return $http.post([urlBase, 'SignUp', ''].join('/'), usuario);
        return this.http
            .post([this.urlBase, 'SignUp', ''].join('/'), usuario)
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.Update = function (telefono, usuario) {
        //return $http.put([urlBase,'Doctor',telefono].join('/'), usuario);
        return this.http
            .put([this.urlBase, 'Doctor', telefono].join('/'), usuario)
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.LoadAddress = function (telefono) {
        //return $http.get([urlBase,'Doctor',telefono,'Empresa'].join('/'));
        return this.http
            .get([this.urlBase, 'Doctor', telefono, 'Empresa'].join('/'))
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.UpdateAddress = function (Id, direccion) {
        //return $http.put([urlBase,'Empresa',Id].join('/'), direccion);
        return this.http
            .put([this.urlBase, 'Empresa', Id].join('/'), direccion)
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.InsertAddress = function (direccion) {
        //return $http.post([urlBase,'Empresa'].join('/'), direccion);
        return this.http
            .post([this.urlBase, 'Empresa'].join('/'), direccion)
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.DeleteAddress = function (Id) {
        //return $http.delete([urlBase,'Empresa',Id].join('/'));
        return this.http
            .delete([this.urlBase, 'Empresa', Id].join('/'))
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.GetOrders = function (Telefono) {
        //return $http.get([urlBase,'usuarios',Telefono,'pedidos'].join('/'));
        return this.http
            .get([this.urlBase, 'usuarios', Telefono, 'pedidos'].join('/'))
            .map(function (res) { return res.json(); });
    };
    //obtener pedidos panoramax
    SignUpFactory.prototype.GetOrdersPanoramax = function (Id) {
        //return $http.get([urlBase,'Doctor',Id,'GetOrdersPanoramax'].join('/'));
        return this.http
            .get([this.urlBase, 'Doctor', Id, 'GetOrdersPanoramax'].join('/'))
            .map(function (res) { return res.json(); });
    };
    //inserta paciente panoramax
    SignUpFactory.prototype.InsertPaciente = function (paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
            .post([this.urlBase, 'Paciente'].join('/'), paciente)
            .map(function (res) { return res.json(); });
    };
    ;
    //inserta paciente panoramax
    SignUpFactory.prototype.UpdatePaciente = function (Id, paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
            .put([this.urlBase, 'Paciente', Id].join('/'), paciente)
            .map(function (res) { return res.json(); });
    };
    ;
    //inserta paciente panoramax
    SignUpFactory.prototype.GetPaciente = function (paciente) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
            .get([this.urlBase, 'Paciente'].join('/'), paciente)
            .map(function (res) { return res.json(); });
    };
    ;
    //inserta paciente panoramax
    SignUpFactory.prototype.GetPacienteId = function (Id) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
            .get([this.urlBase, 'Paciente', Id].join('/'))
            .map(function (res) { return res.json(); });
    };
    ;
    //inserta paciente panoramax
    SignUpFactory.prototype.DeletePacienteId = function (Id) {
        //return $http.post([urlBase,'Paciente'].join('/'), paciente);
        return this.http
            .delete([this.urlBase, 'Paciente', Id].join('/'))
            .map(function (res) { return res.json(); });
    };
    ;
    //inserta paciente panoramax
    SignUpFactory.prototype.CargarDetallesPorSolicitudExamen = function (Id) {
        //return $http.get([urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'), Id);
        return this.http
            .get([this.urlBase, 'DetallesExamenes', Id, 'CargarDetallesPorSolicitudExamen'].join('/'))
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.filterItems = function (searchTerm) {
        return this.http
            .get([this.urlBase, 'Paciente', searchTerm, 'likename'].join('/'))
            .map(function (res) { return res.json(); });
    };
    SignUpFactory.prototype.getResults = function (nombre) {
        //return $http.get([urlBase,'DetallesExamenes',Id,'CargarDetallesPorSolicitudExamen'].join('/'), Id);
        return this.http
            .get([this.urlBase, 'Paciente', nombre, 'likename'].join('/'))
            .map(function (res) { return res.json().filter(function (item) { return item.name.toLowerCase().startsWith(nombre.toLowerCase()); }); });
    };
    SignUpFactory.prototype.Validar = function () {
    };
    SignUpFactory = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [JwtHttp])
    ], SignUpFactory);
    return SignUpFactory;
}());
export { SignUpFactory };
//# sourceMappingURL=SignUpFactory.js.map
